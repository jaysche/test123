var ShipmentHarness = {
    request_active: false,

    init: function() {
        $('#harness_btn_fetch').click(function() {
            // Fetch our data
            var shipping_data = {
                carrier: $('#input_carrier_select option:selected').val(),
                package_weight: $('#input_package_weight').val(),
                package_length: $('#input_package_length').val(),
                package_width: $('#input_package_width').val(),
                package_height: $('#input_package_height').val(),

                shipping_company: $('#input_shipping_company ').val(),
                shipping_phone: $('#input_shipping_phone').val(),
                shipping_address_1: $('#input_shipping_address_1').val(),
                shipping_address_2: $('#input_shipping_address_2').val(),
                shipping_city: $('#input_shipping_city').val(),
                shipping_state: $('#input_shipping_state').val(),
                shipping_zipcode: $('#input_shipping_zipcode').val()
            };

            // Validate our form data

            // if validation passes, show our loader

            ShipmentHarness.fetch_shipping_response(shipping_data);
        });
    },
    fetch_shipping_response: function(shipping_data) {
        ShipmentHarness.request_active = true;
        $('#carrier_package_label_creation .request_loader').removeClass('hide');

        $.ajax({
            url: 'shipment/packagelabel.json',
            type: 'POST',
            data: ({ shipping_data: JSON.stringify(shipping_data) }),
            success: function(response_data) {
                ShipmentHarness.request_active = false;
                $('#carrier_package_label_creation .request_loader').addClass('hide');

                $.each(response_data, function() {
                    $('#carrier_package_label_response_trk_main').html(this.trk_main);
                    $('#carrier_package_label_response_charges').html(this.charges);
                    $.each(this.pkgs, function() {
                        var src = 'data:image/'+ this.label_fmt +';base64,'+this.label_img;
                        $('<img />', { 'src': src, 'height': '320px', 'width': '560px', 'class': '' }).appendTo('#carrier_package_label_response_label');
                    });
                });
                
                TabManager.select_tab('carrier_package_label_results');
            }
        });
    }
};

var TabManager = {
    init: function() {
        TabManager.deselect_content();
        $('.tabs li').click(function() {
            if($(this).hasClass('active')) {
                // we're clicking on an the already active element... do nothing
                return false;
            } else {
                // Get our newly selected tab
                var new_elem = $(this).children('a').attr('href').replace('#', '');
                TabManager.select_tab(new_elem);
            }
            return false;
        });
    },
    select_tab: function(new_elem) {
        // Get our currently selected tab
        var orig_elem = $('.tabs li.active a').attr('href').replace('#', '');
        $('#'+orig_elem).hide();

        $('.tabs li').removeClass('active'); // Remove active class from all list items

        $('a[href="#'+new_elem+'"]').parent().addClass('active'); // select our new tab based on the href value
        $('#'+new_elem).show();
    },
    deselect_content: function() {
        var elem = '';
        $.each($('.tabs li'), function() {
            if(!$(this).hasClass('active')) {
                elem = $(this).children('a').attr('href').replace('#', '');
                $('#'+elem).hide();
            }
        });
    }
}

var App = {
    init: function() {
        TabManager.init();
        ShipmentHarness.init();
    }
}

$(document).ready(function() {
    prettyPrint();
    App.init();
});