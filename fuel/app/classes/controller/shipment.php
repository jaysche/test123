<?php
class Controller_Shipment extends Controller_Rest
{

    public function post_packagelabel() {
//        $this->response(array(
//                             'foo' => Input::get('foo'),
//                             'baz' => array(
//                                 1, 50, 219
//                             ),
//                             'empty' => null
//                        ));


        $shipment = new \Shipping\Shipment('UPS');

        $shipping_data = json_decode(Input::post('shipping_data'));

        $shipment->setParameter('toCompany', $shipping_data->shipping_company);
        $shipment->setParameter('toPhone', $shipping_data->shipping_phone);
        $shipment->setParameter('toAddr1', $shipping_data->shipping_address_1);
        $shipment->setParameter('toCity', $shipping_data->shipping_city);
        $shipment->setParameter('toState', $shipping_data->shipping_state);
        $shipment->setParameter('toCode', $shipping_data->shipping_zipcode);

        $package = new \Shipping\Packages($shipping_data->carrier);
        $package->setParameter('length', $shipping_data->package_length);
        $package->setParameter('width', $shipping_data->package_width);
        $package->setParameter('height', $shipping_data->package_height);
        $package->setParameter('weight', $shipping_data->package_weight);

        $shipment->addPackageToShipment($package);
        $response = $shipment->submitShipment();

        $this->response(array(
            'package_data' => $response
        ));
    }
}

?>