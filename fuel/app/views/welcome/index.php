<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SCLogic Shipment Connector</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php
        echo Asset::css(array('bootstrap-1.2.0.min.css', 'style.css'));
    ?>

    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
</head>

<body>

<div class="topbar">
    <div class="fill">
        <div class="container">
            <h3><a href="#">SCLogic Shipment Connector</a></h3>
            <ul class="nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">

    <section id="about">
        <div class="page-header">
            <h1>About The Connector</h1>
        </div>
        <div class="row">
            <div class="span4 columns">
                <h3>Preface</h3>

                <p>
                    This is a testing harness for the SCLogic shipping connector. Below, you can generate
                    results from all of the main shippers (UPS, USPS, &amp; FedEx) such as time and tracking,
                    rate tracking, label generation, etc. Each service makes a request to the respective shipper
                    api url along with all of the post data you will be passing through. This connector is entirely RESTful,
                    so making further requests is completely simple.
                </p>
                <p>
                    Although the harness will give you results, please keep in mind that throughout this demo page
                    you are utilizing this tool within debug mode. This is a simple boolean flag that can be altered
                    at any time.
                </p>
            </div>
            <div class="span12 columns">
                <h3>Example Shipment Response</h3>
                <pre class="prettyprint linenums">Array (
    [PRIORITY_OVERNIGHT] => 36.21
    [STANDARD_OVERNIGHT] => 22.9
    [FEDEX_2_DAY] => 12.57
    [FEDEX_EXPRESS_SAVER] => 10.7
    [FEDEX_GROUND] => 5.65
)               </pre>
            </div>
        </div>
    </section>

    <section id="harness">
        <div class="page-header">
            <h1>Creating a Shipment and Shipping Label</h1>
        </div>
        <div class="row">
            <div class="span4 columns">
                <h3>Understanding</h3>
                <p>
                    Generating a shipment and shipping label is as simple as filling in the
                    forms to the right and clicking generate.  The data is then gathered
                    asynchronously and automatically displayed.  Shipping labels are returned
                    as base64 encoded strings, which can be displayed by adding <code>data:image/gif;base64,[base64string]</code>
                    into your image's <code>src</code> tag.
                </p>
            </div>
            <div class="span12 columns">
                <ul class="tabs">
                    <li class="active"><a href="#carrier_package_label_creation">Params</a></li>
                    <li><a href="#carrier_package_label_results">Results</a></li>
                </ul>
                <div id="carrier_package_label_creation">
                    <fieldset id="carrier_package_modifiers">
                        <legend>Carrier &amp; Package Modifiers</legend>
                        <div class="clearfix">
                            <label for="input_carrier_select">Carrier</label>
                            <div class="input">
                                <select name="normalSelect" id="input_carrier_select">
                                    <option>UPS</option>
                                    <option>FedEx</option>
                                    <option>USPS</option>
                                    <option>DHL</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_package_weight">Package Weight</label>

                            <div class="input">
                                <input class="mini required" id="input_package_weight" name="input_package_weight " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_package_length">Package Length</label>

                            <div class="input">
                                <input class="mini" id="input_package_length" name="input_package_length " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_package_width">Package Width</label>

                            <div class="input">
                                <input class="mini" id="input_package_width" name="input_package_width " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_package_height">Package Height</label>

                            <div class="input">
                                <input class="mini" id="input_package_height" name="input_package_height " size="30"
                                       type="text">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset id="address_modifiers">
                        <legend>Address Modifiers</legend>
                        <div class="clearfix">
                            <label for="input_shipping_company">Company</label>

                            <div class="input">
                                <input class="xlarge" id="input_shipping_company" name="input_shipping_company " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_phone">Phone</label>

                            <div class="input">
                                <input class="xlarge" id="input_shipping_phone" name="input_shipping_phone " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_address_1">Address 1</label>

                            <div class="input">
                                <input class="xlarge" id="input_shipping_address_1" name="input_shipping_address_1 "
                                       size="30" type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_address_2">Address 2</label>

                            <div class="input">
                                <input class="xlarge" id="input_shipping_address_2" name="input_shipping_address_2 "
                                       size="30" type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_city">City</label>

                            <div class="input">
                                <input class="xlarge" id="input_shipping_city" name="input_shipping_city " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_state">State</label>

                            <div class="input">
                                <input class="mini" id="input_shipping_state" name="input_shipping_state " size="30"
                                       type="text">
                            </div>
                        </div>
                        <div class="clearfix">
                            <label for="input_shipping_zipcode">Zipcode</label>

                            <div class="input">
                                <input class="mini" id="input_shipping_zipcode" name="input_shipping_zipcode " size="30"
                                       type="text">
                            </div>
                        </div>
                    </fieldset>
                    <div class="actions">
                        <?php echo Asset::img('ajax-loader.gif', array('class' => 'request_loader hide')) ?>
                        <button id="harness_btn_fetch" class="btn primary">Fetch</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </div>
                <div id="carrier_package_label_results">
                    <fieldset id="carrier_package_label_response">
                        <legend>Carrier &amp; Package Response</legend>
                        <div class="clearfix">
                            <label>Charges</label>
                            <div class="input">
                                <span class="uneditable-input" id="carrier_package_label_response_charges"></span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <label>Tracking Num</label>
                            <div class="input">
                                <span class="uneditable-input" id="carrier_package_label_response_trk_main"></span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <label>Package Label</label>
                            <div class="input">
                                <div class="well" id="carrier_package_label_response_label"></div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <p>&copy; SCLogic 2011</p>
    </footer>

</div>
<!-- /container -->

<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>

<!-- scripts concatenated and minified via build script -->
<?php echo Asset::js(array('prettify/prettify.js', 'jquery-ui-1.8.16.custom.min.js', 'json2.js', 'script.js'), array("defer")); ?>
<!-- end scripts -->


<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
mathiasbynens.be/notes/async-analytics-snippet -->
<!--<script>-->
<!--    var _gaq = [-->
<!--        ['_setAccount','UA-XXXXX-X'],-->
<!--        ['_trackPageview'],-->
<!--        ['_trackPageLoadTime']-->
<!--    ];-->
<!--    (function(d, t) {-->
<!--        var g = d.createElement(t),s = d.getElementsByTagName(t)[0];-->
<!--        g.src = ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js';-->
<!--        s.parentNode.insertBefore(g, s)-->
<!--    }(document, 'script'));-->
<!--</script>-->

<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
chromium.org/developers/how-tos/chrome-frame-getting-started -->
<!--[if lt IE 7 ]>
<script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
<script defer>window.attachEvent('onload', function() {
    CFInstall.check({mode:'overlay'})
})</script>
<![endif]-->

</body>
</html>