<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.0
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2011 Fuel Development Team
 * @link       http://fuelphp.com
 */

// Add namespace, necessary if you want the autoloader to be able to find classes
Autoloader::add_namespace('Shipping', __DIR__.'/classes/');

// Add as core namespace
Autoloader::add_core_namespace('Shipping');

Autoloader::add_classes(array(
    'Shipping\\Shipping'                    => __DIR__.'/classes/shipping.php',

    'Shipping\\Address_Validate'			=> __DIR__.'/classes/shipping/address_validate.php',
	'Shipping\\Packages'				   	=> __DIR__.'/classes/shipping/packages.php',
	'Shipping\\Pickup'				   	    => __DIR__.'/classes/shipping/pickup.php',
    'Shipping\\Queue'			            => __DIR__.'/classes/shipping/queue.php',
	'Shipping\\Rates'					   	=> __DIR__.'/classes/shipping/rates.php',
	'Shipping\\Shipment'				   	=> __DIR__.'/classes/shipping/shipment.php',
    'Shipping\\Tracking'				    => __DIR__.'/classes/shipping/tracking.php',
	'Shipping\\Transit'					 	=> __DIR__.'/classes/shipping/transit_time.php',
	'Shipping\\Void'					 	=> __DIR__.'/classes/shipping/void.php',

	'Shipping\\Carrier_UPS'					=> __DIR__.'/classes/shipping/carrier/ups.php',

    'Shipping\\Util_XML_Builder'			=> __DIR__.'/classes/shipping/util/xmlbuilder.php',
    'Shipping\\Util_UPS_XML_Parser'			=> __DIR__.'/classes/shipping/util/ups_xml_parser.php',
));

/* End of file bootstrap.php */