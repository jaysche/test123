<?php

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(
    // 1 for Debug mode, 0 for normal operations
    // This also changes from testing to production mode
	'debugMode' => '1',
    
    // Your company name
    'shipper' => 'Federal Resources Supply Co.',

    // Key shipping contact individual at your company
    'shipContact' => "Josh Sullivan",

    'shipAddr1' => '109 Shamrock Rd',
    'shipAddr2' => '',
    
    'shipCity' => 'Chester',

    // the two-letter State or Province code
    // ex. MT => Montana, ON => Ontario
    'shipState' => 'MD',

    // The Zip or Postal code
    'shipCode' => '21619',

    // The two-letter country code
    'shipCountry' => 'US',

    // Phone number in this format: 1234567890
    'shipPhone' => '1231231234',
    'toCountry' => 'US',
);
