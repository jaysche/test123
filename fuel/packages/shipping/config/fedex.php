<?php

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(
    // Your FedEx developer key
    'key' => '',

    // Your FedEx developer password
    'password' => '',

    // Your FedEx accountNumber
    'accountNumber' => '',

    // Your FedEx meter number
    'meterNumber' => '',

    // Allowed packaging types:
    // FEDEX_10KG_BOX
    // FEDEX_25KG_BOX
    // FEDEX_BOX
    // FEDEX_ENVELOPE
    // FEDEX_PAK
    // FEDEX_TUBE
    // YOUR_PACKAGING
    'packagingType' => '',

    // The two possible weight units are LB and KG
    'weightUnit' => 'LB',

    // The two possible length units are IN and CM
    'lengthUnit' => 'IN',

    // EUROPE_FIRST_INTERNATIONAL_PRIORITY
    // FEDEX_1_DAY_FREIGHT
    // FEDEX_2_DAY
    // FEDEX_2_DAY_FREIGHT
    // FEDEX_3_DAY_FREIGHT
    // FEDEX_EXPRESS_SAVER
    // FEDEX_GROUND
    // FIRST_OVERNIGHT
    // GROUND_HOME_DELIVERY
    // INTERNATIONAL_ECONOMY
    // INTERNATIONAL_ECONOMY_FREIGHT
    // INTERNATIONAL_FIRST
    // INTERNATIONAL_PRIORITY
    // INTERNATIONAL_PRIORITY_FREIGHT
    // PRIORITY_OVERNIGHT
    // SMART_POST
    // STANDARD_OVERNIGHT
    // FEDEX_FREIGHT
    // FEDEX_NATIONAL_FREIGHT
    'service' => '',

    // REGULAR_PICKUP
    // REQUEST_COURIER
    // DROP_BOX
    // BUSINESS_SERVICE_CENTER
    // STATION
    'dropoffType' => '',

    // COLLECT
    // RECIPIENT
    // SENDER
    // THIRD_PARTY
    'paymentType' => 'SENDER',

    // COMMON2D
    // LABEL_DATA_ONLY
    'labelFormatType' => '',

    // DPL
    // EPL2
    // PDF
    // PNG
    // ZPLII
    'imageType' => 'PNG',

    // PAPER_4X6
    // PAPER_4X8
    // PAPER_4X9
    // PAPER_7X4.75
    // PAPER_8.5X11_BOTTOM_HALF_LABEL
    // PAPER_8.5X11_TOP_HALF_LABEL
    // STOCK_4X6
    // STOCK_4X6.75_LEADING_DOC_TAB
    // STOCK_4X6.75_TRAILING_DOC_TAB
    // STOCK_4X8
    // STOCK_4X9_LEADING_DOC_TAB
    // STOCK_4X9_TRAILING_DOC_TAB
    'labelStockType' => 'PAPER_4X6',

    // BILL_OF_LADING
    // COD_RETURN_TRACKING_NUMBER
    // CUSTOMER_AUTHORIZATION_NUMBER
    // CUSTOMER_REFERENCE
    // DEPARTMENT
    // FREE_FORM_REFERENCE
    // GROUND_SHIPMENT_ID
    // GROUND_MPS
    // INVOICE
    // PARTNER_CARRIER_NUMBER
    // PART_NUMBER
    // PURCHASE_ORDER
    // RETURN_MATERIALS_AUTHORIZATION
    // TRACKING_CONTROL_NUMBER
    // TRACKING_NUMBER_OR_DOORTAG
    // SHIPPER_REFERENCE
    // STANDARD_MPS
    'trackingIdType' => 'TRACKING_NUMBER_OR_DOORTAG',

);
