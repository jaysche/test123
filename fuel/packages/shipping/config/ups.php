<?php

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(
    // Your UPS Developer license
    // your UPS XML Access Key TODO: Insert link to get one
    'license' => '987654654321',

    // your UPS Developer username
    'username' => '*******',

    // your ups Developer password
    'password' => '*******',

    // Make sure addresses are valid before label creation
    // validate, nonvalidate
    'verifyAddress' => 'nonvalidate',

    // The following variables govern the way the system functions
    // Options
    // ZPL - Zebra UPS Thermal Printers
    // EPL - Eltron UPS Thermal Printers
    // GIF - Image based, desktop inkjet printers
    // STARPL
    // SPL
    'labelPrintMethodCode' => 'GIF',

    // Used when printing GIF images
    'httpUserAgent' => 'Mozilla/4.5',

    // Only valid option for ZPL, EPL, STARPL, and SPL is 4
    // When using inches use whole numbers only
    'labelHeight' => '4',

    // Options are 6 or 8 inches
    'labelWidth' => '6',

    // Options
    // GIF - A gif image
    'labelImageFormat' => 'gif',

    // The following variables are for your UPS account
    // They typically don't change from shipment to shipment, although,
    // you may set any of them directly.
    // Your UPS Account number
    'accountNumber' => '9T443T',

    // Options
    // 01 - Daily Pickup
    // 03 - Customer Counter
    // 06 - One Time Pickup
    // 07 - On Call Air
    // 11 - Suggested Retail Rates
    // 19 - Letter Center
    // 20 - Air Service Center
    'PickupType' => '01',

    // LBS or KGS
    'weightUnit' => 'LBS',

    // IN, or CM
    'lengthUnit' => 'IN',

    // See the ups manual for a list of all currency types
    'insuredCurrency' => 'USD',

    // two-letter country code
    'toCountryCode' => 'US',

    // The following variables set the defaults for individual shipments
    // you may set them here to save time, or you may set them explicitly
    // each time you use the classes.
    'shipmentDescription' => 'My Shipment',

    // Options
    // 01 - UPS Next Day Air
    // 02 - UPS Second Day Air
    // 03 - UPS Ground
    // 07 - UPS Worldwide Express
    // 08 - UPS Worldwide Expedited
    // 11 - UPS Standard
    // 12 - UPS Three-Day Select
    // 13 - Next Day Air Saver
    // 14 - UPS Next Day Air Early AM
    // 54 - UPS Worldwide Express Plus
    // 59 - UPS Second Day Air AM
    // 65 - UPS Saver
    'service' => '03',

    // Options
    // 01 - UPS Letter
    // 02 - Your Packaging
    // 03 - Tube
    // 04 - PAK
    // 21 - Express Box
    // 24 - 25KG Box
    // 25 - 10KG Box
    // 30 - Pallet
    // 2a - Small Express Box
    // 2b - Medium Express Box
    // 2c - Large Express Box
    'packagingType' => '02',

    'packageDescription' => 'Rate',

    // Set '0' for commercial '1' for residential
    'residentialAddressIndicator' => '0',

    // Set '0' for retail rates '1' for negotiated
    // You must turn this on with your UPS account rep
    'negotiatedRates' => '0',

    // Options
    // AJ Accounts Receivable Customer Account
    // AT Appropriation Number
    // BM Bill of Lading Number
    // 9V Collect on Delivery (COD) Number
    // ON Dealer Order Number
    // DP Department Number
    // 3Q Food and Drug Administration (FDA) Product Code
    // IK Invoice Number
    // MK Manifest Key Number
    // MJ Model Number
    // PM Part Number
    // PC Production Code
    // PO Purchase Order Number
    // RQ Purchase Request Number
    // RZ Return Authorization Number
    // SA Salesperson Number
    // SE Serial Number
    // ST Store Number
    // TN Transaction Reference Number
    'referenceCode' => 'IK',

    // Options
    // 2 - UPS Print and Mail (PNM)
    // 3 - UPS Return Service 1-Attempt (RS1)
    // 5 - UPS Return Service 3-Attempt (RS3)
    // 8 - UPS Electronic Return Label (ERL)
    // 9 - UPS Print Return Label (PRL)
    'returnCode' => '',
);
