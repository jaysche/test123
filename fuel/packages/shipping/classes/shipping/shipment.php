<?php

//// RocketShipIt Config
//require('config.php');


namespace Shipping;

/*
 * Main Shipping class for producing labels and notifying carriers of pickups.
 *
 * This class is a wrapper for use with all carriers to produce labels for
 * shipments.  Valid carriers are: UPS, USPS, and FedEx.
 */
class Shipment
{

    var $OKparams;
    var $customsLines;

    function __construct($carrier, $license = '', $username = '', $password = '')
    {
        Shipping::rocketshipit_validateCarrier($carrier);
        $carrier = strtoupper($carrier);
        $this->carrier = $carrier;
        $this->OKparams = Shipping::rocketshipit_getOKparams($carrier);
        $this->customsLines = Array();

        // Set up core class and grab carrier-specific defaults that are 
        // unique to the current carrier
        if ($carrier == "UPS") {
            $this->core = new \Shipping\Carrier_UPS($license, $username, $password); // This class depends on ups
            
            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }

            if ($license != '') {
                $this->core->license = $license;
            }
            if ($username != '') {
                $this->core->username = $username;
            }
            if ($password != '') {
                $this->core->password = $password;
            }
        }
        if ($carrier == "USPS") {
            $this->core = new usps();

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
        if ($carrier == 'FEDEX') {
            $this->core = new \Shipping\Carrier_FedEx();

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
        if ($carrier == 'STAMPS') {
            $this->core = new stamps(); // This class depends on stamps

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
    }

    /**
     * Sets paramaters to be used in {@link Shipping_Shipment() Shipping_Shipment}.
     *
     * Only valid parameters are accepted.
     * @see getOKparams()
     *
     * @param $param
     * @param $value
     * @return void
     */
    public function setParameter($param, $value)
    {
        $value = Shipping::rocketshipit_getParameter($param, $value, $this->carrier);
        $this->{$param} = $value;
    }

    /**
     * This is a wrapper to create a running package for each carrier.
     *
     * This is used to add packages to a shipment for any carrier.
     * You use the {@link RocketShipPackage} class to create a package
     * object.
     *
     * @param $packageObj
     * @return bool
     */
    function addPackageToShipment($packageObj)
    {
        switch ($this->carrier) {
            case "UPS":
                return $this->addPackageToUPSshipment($packageObj);
            case "USPS":
                return $this->addPackageToUSPSshipment($packageObj);
            case 'FEDEX':
                return $this->addPackageToFEDEXshipment($packageObj);
            case 'STAMPS':
                return $this->addPackageToSTAMPSshipment($packageObj);
            default:
                return false;
        }
    }

    /**
     * This is a wrapper to create a running customs document for each carrier.
     *
     * @param $packageObj
     * @return bool
     */
    function addCustomsLineToShipment($packageObj)
    {
        switch ($this->carrier) {
            case 'STAMPS':
                return $this->addCustomsLineToSTAMPSshipment($packageObj);
            case 'FEDEX':
                return $this->addCustomsLineToFEDEXshipment($packageObj);
            default:
                return false;
        }
    }

    function addCustomsLineToFEDEXshipment($customs) {
        if (!isset($this->core->customsObject)) {
            $this->core->customsObject = new xmlBuilder(true);
        }

        $xml = $this->core->customsObject;

        $xml->push('ns:Commodities');
            $xml->element('ns:NumberOfPieces', $customs->customsNumberOfPieces);
            $xml->element('ns:CountryOfManufacture', $customs->countryOfManufacture);
            $xml->push('ns:Weight');
                $xml->element('ns:Units', $customs->weightUnit);
                $xml->element('ns:Value', $customs->customsWeight);
            $xml->pop(); // end Weight
        $xml->pop(); // end Commodities

        return $this->core->customsObject = $xml;
    }


    private function addPackageToUPSshipment($packageObj) {
        $package = $packageObj;

        if (!isset($this->core->packagesObject)) {
            $this->core->packagesObject = new \Shipping\Util_XML_Builder(true);
        }

        $xml = $this->core->packagesObject;

        $xml->push('Package');
        if ($this->packageDescription != '') {
            $xml->element('Description', $this->packageDescription);
        }
        if ($package->packagingType != '') {
            $xml->push('PackagingType');
            $xml->element('Code', $package->packagingType);
            $xml->pop(); // end PackagingType
        }
        if ($package->length != '') {
            if ($package->packagingType != '01') {
                $xml->push('Dimensions');
                $xml->push('UnitOfMeasurement');
                if ($this->lengthUnit != '') {
                    $xml->element('Code', $package->lengthUnit);
                }
                $xml->pop(); // end UnitOfMeasurement
                if ($package->length != '') {
                    $xml->element('Length', $package->length);
                }
                if ($package->width != '') {
                    $xml->element('Width', $package->width);
                }
                if ($package->height != '') {
                    $xml->element('Height', $package->height);
                }
                $xml->pop(); // end Dimensions
            }
        }
        if ($package->weight != '') {
            $xml->push('PackageWeight');
            $xml->element('Weight', $package->weight);
            $xml->pop(); // end PackageWeight
        }

        if ($package->referenceValue && $package->referenceCode != '') {
            $xml->push('ReferenceNumber');
            $xml->element('Code', $package->referenceCode);
            $xml->element('Value', $package->referenceValue);
            $xml->pop(); // end ReferenceNumber
        }

        if ($package->referenceValue2 && $package->referenceCode2 != '') {
            $xml->push('ReferenceNumber');
            $xml->element('Code', $package->referenceCode2);
            $xml->element('Value', $package->referenceValue2);
            $xml->pop(); // end ReferenceNumber
        }

        if ($package->monetaryValue != '' || $package->flexibleAccess != '' || $package->signatureType != '' || $package->codAmount != '') {
            $xml->push('PackageServiceOptions');
            if ($package->flexibleAccess != '') {
                $xml->element('ReturnsFlexibleAccessIndicator', '1');
            }
            if ($package->signatureType != '') {
                $xml->push('DeliveryConfirmation');
                $xml->element('DCISType', $package->signatureType);
                $xml->pop(); // end DeliveryConfirmation
            }
            if ($package->monetaryValue != '') {
                $xml->push('InsuredValue');
                $xml->element('CurrencyCode', $package->insuredCurrency);
                $xml->element('MonetaryValue', $package->monetaryValue);
                $xml->pop(); // End Insured Value
            }
            if ($package->codAmount != '') {
                $xml->push('COD');
                $xml->element('CODCode', '3');
                $xml->element('CODFundsCode', $package->codFundType);
                $xml->push('CODAmount');
                $xml->element('CurrencyCode', $package->insuredCurrency);
                $xml->element('MonetaryValue', $package->codAmount);
                $xml->pop(); // end CODAmount
                $xml->pop(); // end COD
            }
            $xml->pop(); // end PackageServiceOptions
        }

        $xml->pop(); // end Package
        $this->core->packagesObject = $xml;

        return true;
    }

    function addPackageToFEDEXShipment($package) {
        if (!isset($this->core->packagesObject)) {
            $this->core->packagesObject = new xmlBuilder(true);
        }

        $xml = $this->core->packagesObject;

        $xml->push('ns:RequestedPackageLineItems');
            $xml->element('ns:SequenceNumber','1');
            if ($this->insuredValue != '' && $this->insuredCurrency != '') {
                $xml->push('ns:InsuredValue');
                    $xml->element('ns:Currency', $this->insuredCurrency);
                    $xml->element('ns:Amount', $this->insuredValue);
                $xml->pop(); // end InsuredValue
            }
            $xml->push('ns:Weight');
                $xml->element('ns:Units',$package->weightUnit);
                $xml->element('ns:Value',$package->weight);
            $xml->pop(); // end Weight
            if ($package->length != '') {
                $xml->push('ns:Dimensions');
                    if ($package->length != '') {
                        $xml->element('ns:Length',$package->length);
                    }
                    if ($package->width != '') {
                        $xml->element('ns:Width',$package->width);
                    }
                    if ($package->height != '') {
                        $xml->element('ns:Height',$package->height);
                    }
                    $xml->element('ns:Units',$package->lengthUnit);
                $xml->pop(); // end Dimensions
            }
            if ($this->signatureType != '') {
                $xml->push('ns:SpecialServicesRequested');
                    $xml->element('ns:SpecialServiceTypes', 'SIGNATURE_OPTION');
                    $xml->push('ns:SignatureOptionDetail');
                        $xml->element('ns:OptionType', $this->signatureType);
                    $xml->pop();
                $xml->pop(); // end ShipmentSpecialServicesRequested
            }
        $xml->pop(); // end RequestedPackageLineItems

        $this->core->packagesObject = $xml;
        $this->packageCount++;
        return true;
    }

    function buildUPSshipmentXML()
    {
        $xml = $this->core->xmlObject;

        $xml->push('ShipmentConfirmRequest');
        $xml->push('Request');
        $xml->push('TransactionReference');
        $xml->element('CustomerContext', 'Federal Resources Shipping Co.');
        $xml->element('XpciVersion', '1.0001');
        $xml->pop();
        $xml->element('RequestAction', 'ShipConfirm');
        if ($this->verifyAddress != '') {
            $xml->element('RequestOption', $this->verifyAddress);
        }
        $xml->pop(); // end Request
        $xml->push('Shipment');
        if ($this->shipmentDescription != '') {
            $xml->element('Description', $this->shipmentDescription);
        }
        if ($this->returnCode != '') {
            $xml->push('ReturnService');
            $xml->element('Code', $this->returnCode);
            $xml->pop(); // end ReturnService
        }
        if ($this->returnCode == '8' || $this->codAmount != '') {
            $xml->push('ShipmentServiceOptions');
            if ($this->codAmount != '') {
                $xml->push('COD');
                $xml->element('CODCode', '3');
                $xml->element('CODFundsCode', $this->codFundType);
                $xml->push('CODAmount');
                $xml->element('CurrencyCode', $this->insuredCurrency);
                $xml->element('MonetaryValue', $this->codAmount);
                $xml->pop(); // end CODAmount
                $xml->pop(); // end COD
            }
            if ($this->returnCode == '8') {
                $xml->push('LabelDelivery');
                $xml->push('EMailMessage');
                $xml->element('EMailAddress', $this->returnEmailAddress);
                $xml->element('UndeliverableEMailAddress', $this->returnUndeliverableEmailAddress);
                $xml->element('FromEMailAddress', $this->returnFromEmailAddress);
                $xml->element('FromName', $this->returnEmailFromName);
                $xml->pop(); // end EMailMessage
                $xml->pop(); // end LabelDelivery
            }
            $xml->pop(); // end ShipmentServiceOptions
        }
        $xml->push('Shipper');
        if ($this->shipper != '') {
            $xml->element('Name', $this->shipper);
        }
        if ($this->shipContact != '') {
            $xml->element('AttentionName', $this->shipContact);
        }
        if ($this->accountNumber != '') {
            $xml->element('ShipperNumber', $this->accountNumber);
        }
        if ($this->shipPhone != '') {
            $xml->element('PhoneNumber', $this->shipPhone);
        }
        $xml->push('Address');
        if ($this->shipAddr1 != '') {
            $xml->element('AddressLine1', $this->shipAddr1);
        }
        if ($this->shipAddr2 != '') {
            $xml->element('AddressLine2', $this->shipAddr2);
        }
        if ($this->shipCity != '') {
            $xml->element('City', $this->shipCity);
        }
        if ($this->shipState != '') {
            $xml->element('StateProvinceCode', $this->shipState);
        }
        if ($this->shipCode != '') {
            $xml->element('PostalCode', $this->shipCode);
        }
        if ($this->shipCountry != '') {
            $xml->element('CountryCode', $this->shipCountry);
        }
        $xml->pop(); // end Address
        $xml->pop(); // end Shipper 
        $xml->push('ShipTo');
        if ($this->toCompany == "" && $this->toName == "") {
            exit("Must have either name or company set for shipping address");
        }
        if ($this->toCompany != '') {
            $xml->element('CompanyName', $this->toCompany);
        } else {
            $xml->element('CompanyName', $this->toName);
        }
        if ($this->toName != '' && $this->toCompany != '') {
            $xml->element('AttentionName', $this->toName);
        } else {
            if ($this->toCountry != $this->shipCountry) {
                $xml->element('AttentionName', $this->toName);
            }
        }
        if ($this->toPhone != '') {
            $xml->element('PhoneNumber', $this->toPhone);
        }
        $xml->push('Address');
        if ($this->toAddr1 != '') {
            $xml->element('AddressLine1', $this->toAddr1);
        }
        if ($this->toAddr2 != '') {
            $xml->element('AddressLine2', $this->toAddr2);
        }
        $xml->element('City', $this->toCity);
        if ($this->toState != '') {
            $xml->element('StateProvinceCode', $this->toState);
        }
        if ($this->toCountry != '') {
            $xml->element('CountryCode', $this->toCountry);
        }
        if ($this->toCode != '') {
            $xml->element('PostalCode', $this->toCode);
        }
        if ($this->residentialAddressIndicator == '1') {
            $xml->emptyelement('ResidentialAddress');
        }
        $xml->pop(); // end Address
        $xml->pop(); // end ShipTo
        if ($this->fromName != '') {
            $xml->push('ShipFrom');
            $xml->element('CompanyName', $this->fromName);
            $xml->push('Address');
            $xml->element('AddressLine1', $this->fromAddr1);
            if ($this->fromAddr2 != '') {
                $xml->element('AddressLine2', $this->fromAddr2);
            }
            if ($this->fromAddr3 != '') {
                $xml->element('AddressLine3', $this->fromAddr3);
            }
            $xml->element('City', $this->fromCity);
            $xml->element('StateProvinceCode', $this->fromState);
            $xml->element('CountryCode', $this->fromCountry);
            $xml->element('PostalCode', $this->fromCode);
            $xml->pop(); // end Address
            $xml->pop(); // end ShipFrom
        }
        if ($this->referenceValue && $this->referenceCode != '') {
            $xml->push('ReferenceNumber');
            $xml->element('Code', $this->referenceCode);
            $xml->element('Value', $this->referenceValue);
            $xml->pop(); // end ReferenceNumber
        }
        $xml->push('Service');
        if ($this->service != '') {
            $xml->element('Code', $this->service);
        }
        if ($this->serviceDescription != '') {
            $xml->element('Description', $this->serviceDescription);
        }
        $xml->pop(); // end Service 
        $xml->push('PaymentInformation');
        if ($this->billThirdParty) {
            $xml->push('BillThirdParty');
            $xml->push('BillThirdPartyShipper');
            $xml->element('AccountNumber', $this->thirdPartyAccount);
            $xml->pop(); // end BillThirdPartyShipper
            $xml->pop(); // end BillThirdParty
        } else {
            $xml->push('Prepaid');
            $xml->push('BillShipper');
            $xml->element('AccountNumber', $this->accountNumber);
            $xml->pop(); // end BillShipper 
            $xml->pop(); // end Prepaid
        }
        $xml->pop(); // end PaymentInformation
        if ($this->monetaryValue != '') {
            $xml->push('InvoiceLineTotal');
            $xml->element('MonetaryValue', $this->monetaryValue);
            $xml->pop(); // end InvoiceLineTotal
        }
        if ($this->saturdayDelivery != '') {
            $xml->push('ShipmentServiceOptions');
            $xml->element('SaturdayDelivery', $this->saturdayDelivery);
            $xml->pop(); // end ShipmentServiceOptions
        }

        // If negotiated rates have been requested
        if ($this->negotiatedRates == '1') {
            $xml->push('RateInformation');
            $xml->element('NegotiatedRatesIndicator', '1');
            $xml->pop(); // close RateInformation
        }

        $xmlString = $xml->getXml();

        $xmlString .= $this->core->packagesObject->getXml();

        $xmlString .= '</Shipment>' . "
";

        $xml = new \Shipping\Util_XML_Builder(true);

        $xml->push('LabelSpecification');
        $xml->push('LabelPrintMethod');
        if ($this->labelPrintMethodCode != '') {
            $xml->element('Code', $this->labelPrintMethodCode);
        }
        if ($this->labelDescription != '') {
            $xml->element('Description', $this->labelDescription);
        }
        $xml->pop(); // end LabelPrintMethod
        if ($this->httpUserAgent != '') {
            $xml->element('HTTPUserAgent', $this->httpUserAgent);
        }
        $xml->push('LabelStockSize');
        if ($this->lengthUnit != '') {
            $xml->element('UnitOfMeasurement', $this->lengthUnit);
        }
        if ($this->labelHeight != '') {
            $xml->element('Height', $this->labelHeight);
        }
        if ($this->labelWidth != '') {
            $xml->element('Width', $this->labelWidth);
        }
        $xml->pop(); // end LabelStockSize
        $xml->push('LabelImageFormat');
        if ($this->labelImageFormat != '') {
            $xml->element('Code', $this->labelImageFormat);
        }
        $xml->pop(); // end LabelImageFormat
        $xml->pop(); // end LabelSpecification

        $labelXmlString = $xml->getXml();

        $xmlString .= $labelXmlString;
        $xmlString .= '</ShipmentConfirmRequest>';

        return $xmlString;
    }

    function buildFEDEXShipmentXml() {
        $xml = $this->core->xmlObject;
        $xml->push('ns:ProcessShipmentRequest', array('xmlns:ns' => 'http://fedex.com/ws/ship/v7',
                                                      'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                                                      'xsi:schemaLocation' => 'http://fedex.com/ws/ship/v7 ShipService v7.xsd'));
        $this->core->xmlObject = $xml;
        $this->core->access();
        $xml = $this->core->xmlObject;

        $xml->push('ns:TransactionDetail');
            $xml->element('ns:CustomerTransactionId','CreatePendingRequest');
        $xml->pop(); // end TransactionDetail
        $xml->push('ns:Version');
            $xml->element('ns:ServiceId','ship');
            $xml->element('ns:Major','7');
            $xml->element('ns:Intermediate','0');
            $xml->element('ns:Minor','0');
        $xml->pop(); // end Version
        $xml->push('ns:RequestedShipment');
            if ($this->shipDate != '') {
                $xml->element('ns:ShipTimestamp', $this->shipDate);
            } else {
                $xml->element('ns:ShipTimestamp',date("c")); // FedEx uses ISO8601 style timestamps
            }
            $xml->element('ns:DropoffType',$this->dropoffType);
            $xml->element('ns:ServiceType',$this->service);
            $xml->element('ns:PackagingType',$this->packagingType);
            $xml->push('ns:Shipper');
                $xml->push('ns:Contact');
                    $xml->element('ns:PersonName',$this->shipContact);
                    $xml->element('ns:CompanyName',$this->shipper);
                    $xml->element('ns:PhoneNumber',$this->shipPhone);
                $xml->pop(); // end Contact
            $xml->push('ns:Address');
                $xml->element('ns:StreetLines',$this->shipAddr1);
                if ($this->shipAddr2 != '') {
                    $xml->element('ns:StreetLines',$this->shipAddr2);
                }
                $xml->element('ns:City',$this->shipCity);
                $xml->element('ns:StateOrProvinceCode',$this->shipState);
                $xml->element('ns:PostalCode',$this->shipCode);
                $xml->element('ns:CountryCode',$this->shipCountry);
            $xml->pop(); // end Address
            $xml->pop(); // end Shipper
            $xml->push('ns:Recipient');
                $xml->push('ns:Contact');
                    $xml->element('ns:PersonName',$this->toName);
                    $xml->element('ns:CompanyName',$this->toCompany);
                    $xml->element('ns:PhoneNumber',$this->toPhone);
                $xml->pop(); // end Contact
            $xml->push('ns:Address');
                $xml->element('ns:StreetLines',$this->toAddr1);
                if ($this->toAddr2 != '') {
                    $xml->element('ns:StreetLines',$this->toAddr2);
                }
                $xml->element('ns:City',$this->toCity);
                $xml->element('ns:StateOrProvinceCode',$this->toState);
                $xml->element('ns:PostalCode',$this->toCode);
                $xml->element('ns:CountryCode',$this->toCountry);
                if ($this->residentialAddressIndicator == '1') {
                    $xml->element('ns:Residential', 'true');
                }
            $xml->pop(); // end Address
            $xml->pop(); // end Recipient
            $xml->push('ns:ShippingChargesPayment');
                $xml->element('ns:PaymentType',$this->paymentType);
                $xml->push('ns:Payor');
                    $xml->element('ns:AccountNumber',$this->accountNumber);
                    $xml->element('ns:CountryCode',$this->shipCountry);
                $xml->pop(); // end Payor
            $xml->pop(); // end ShippingChargesPayment

            if(strtoupper($this->collectOnDelivery) == 'YES'
            || strtoupper($this->holdAtLocation) == 'YES'
            || strtoupper($this->futureDay) == 'YES'
            || strtoupper($this->saturdayDelivery) == 'YES') {

                $xml->push('ns:SpecialServicesRequested');

                    if(strtoupper($this->collectOnDelivery) == 'YES') {
                        $xml->element('ns:SpecialServiceTypes', 'COD');
                        $xml->push('ns:CodDetail');
                            $xml->element('ns:CollectionType', $this->codCollectionType);
                        $xml->pop();
                        $xml->push('ns:CodCollectionAmount');
                            $xml->element('ns:Currency', $this->insuredCurrency);
                            $xml->element('ns:Amount', $this->codCollectionAmount);
                        $xml->pop();
                    }

                    if(strtoupper($this->holdAtLocation) == 'YES') {
                        $xml->element('ns:SpecialServiceTypes', 'HOLD_AT_LOCATION');
                        $xml->push('ns:HoldAtLocationDetail');
                            $xml->element('ns:PhoneNumber', $this->holdPhone);
                            $xml->push('ns:Address');
                                $xml->element('ns:StreetLines', $this->holdStreet);
                                $xml->element('ns:City', $this->holdCity);
                                $xml->element('ns:StateOrProvinceCode', $this->holdState);
                                $xml->element('ns:PostalCode', $this->holdCode);
                                $xml->element('ns:CountryCode', $this->holdCountry);
                                if(strtoupper($this->holdResidential) == 'YES') {
                                    $xml->element('ns:Residential', 'true');
                                }
                            $xml->pop(); // Address
                        $xml->pop(); //HoldAtLocationDetail
                    }

                    if(strtoupper($this->saturdayDelivery) == 'YES') {
                        $xml->element('ns:SpecialServiceTypes', 'SATURDAY_DELIVERY');
                    }

                    if(strtoupper($this->futureDay) == 'YES') {
                        $xml->element('ns:SpecialServiceTypes', 'FUTURE_DAY_SHIPMENT');
                    }

                $xml->pop(); // end ShipmentSpecialServicesRequested
            }

            if ($this->shipCountry != $this->toCountry) {
                $xml->push('ns:InternationalDetail');
                    $xml->element('ns:DocumentContent', $this->customsDocumentContent); // NON_DOCUMENTS or DOCUMENTS_ONLY
                    $xml->push('ns:CustomsValue');
                        $xml->element('ns:Currency', $this->customsCurrency);
                        $xml->element('ns:Amount', $this->customsValue);
                    $xml->pop(); // end CustomsValue
                    $xml->append($this->core->customsObject->getXML());
                $xml->pop(); // end InternationalDetail
            }
            if ($this->service == 'SMART_POST') {
                $xml->push('ns:SmartPostDetail');
                    $xml->element('ns:Indicia', $this->smartPostIndicia);
                    if ($this->smartPostEndorsement != '') {
                        $xml->element('ns:AncillaryEndorsement', $this->smartPostEndorsement);
                    }
                    if ($this->smartPostSpecialServices != '') {
                        $xml->element('ns:SpecialServices', $this->smartPostSpecialServices);
                    }
                    if ($this->smartPostHubId != '') {
                        $xml->element('ns:HubId', $this->smartPostHubId);
                    }
                $xml->pop(); // end SmartPostDetail
            }
            $xml->push('ns:LabelSpecification');
                $xml->element('ns:LabelFormatType',$this->labelFormatType);
                $xml->element('ns:ImageType',$this->imageType);
                $xml->element('ns:LabelStockType',$this->labelStockType);
            $xml->pop(); // end LabelSpecification
            $xml->element('ns:RateRequestTypes','LIST');
            if ($this->shipmentIdentification != '') {
                $xml->push('ns:MasterTrackingId');
                     //$xml->element('TrackingIdType','GROUND');
                    $xml->element('ns:TrackingNumber',$this->shipmentIdentification);
                $xml->pop(); // end MasterTrackingId
            }
            if ($this->packageCount != '') {
                $xml->element('ns:PackageCount',$this->packageCount);
            } else {
                $xml->element('ns:PackageCount','1');
            }
            $xml->element('ns:PackageDetail','INDIVIDUAL_PACKAGES');

            $xml->push('ns:RequestedPackageLineItems');
                if ($this->sequenceNumber != '') {
                    $xml->element('ns:SequenceNumber',$this->sequenceNumber);
                }
                $xml->push('ns:Weight');
                    $xml->element('ns:Units','LB');
                    $xml->element('ns:Value',$this->weight);
                $xml->pop(); // end Weight
                $xml->push('ns:Dimensions');
                    $xml->element('ns:Length', $this->length);
                    $xml->element('ns:Width', $this->width);
                    $xml->element('ns:Height', $this->height);
                    $xml->element('ns:Units', $this->lengthUnit);
                $xml->pop(); // end Dimensions
                if ($this->referenceCode != '' && $this->referenceValue != '') {
                    $xml->push('ns:CustomerReferences');
                        $xml->element('ns:CustomerReferenceType', $this->referenceCode);
                        $xml->element('ns:Value', $this->referenceValue);
                    $xml->pop(); // end CustomerReferences
                }
                if ($this->signatureType != '' || $this->collectOnDelivery == 'YES') {

                    $xml->push('ns:SpecialServicesRequested');

                    if($this->signatureType != '') {
                        $xml->element('ns:SpecialServiceTypes', 'SIGNATURE_OPTION');
                        $xml->push('ns:SignatureOptionDetail');
                            $xml->element('ns:OptionType', $this->signatureType);
                        $xml->pop();
                    }

                    if($this->collectOnDelivery == 'YES') {
                        $xml->push('ns:CodCollectionAmount');
                            $xml->element('ns:Currency', $this->currency);
                            $xml->element('ns:Amount', $this->codCollectionAmount);
                        $xml->pop();
                    }

                    $xml->pop(); // end PackageSpecialServicesRequested
                }
            $xml->pop(); // end RequestedPackageLineItems

        $xml->pop(); // end RequestedShipment


        $xml->pop(); // end CreatePendingShipmentRequest

        $xmlString = $xml->getXml();
        return $xmlString;
    }


    function simplifyUPSResponse($xmlArray)
    {
        if ($xmlArray['ShipmentConfirmResponse']['Response']['ResponseStatusCode']['VALUE'] != "1") {
            return ("Error confirming shipment: " .
                    $xmlArray['ShipmentConfirmResponse']['Response']['Error']['ErrorDescription']['VALUE'] .
                    " (" . $xmlArray['ShipmentConfirmResponse']['Response']['Error']['ErrorCode']['VALUE'] . ")");
        }


        $labelArray = $this->getUPSlabels();
        $a = $labelArray['ShipmentAcceptResponse']['ShipmentResults'];
        $outArr = "";
        $outArr['charges'] = $a['ShipmentCharges']['TotalCharges']['MonetaryValue']['VALUE'];

        // If negotiated rates have been requested
        if ($this->negotiatedRates == '1') {
            $outArr['negotiated_charges'] = $a['NegotiatedRates']['NetSummaryCharges']['GrandTotal']['MonetaryValue']['VALUE'];
        }

        $outArr['trk_main'] = $a['ShipmentIdentificationNumber']['VALUE'];
        if (array_key_exists('TrackingNumber', $a['PackageResults'])) {
            // just a single label
            $outArr['pkgs'][0]['pkg_trk_num'] = $a['PackageResults']['TrackingNumber']['VALUE'];
            $outArr['pkgs'][0]['label_fmt'] = $a['PackageResults']['LabelImage']['LabelImageFormat']['Code']['VALUE'];
            $outArr['pkgs'][0]['label_img'] = $a['PackageResults']['LabelImage']['GraphicImage']['VALUE'];
        } else {
            // multiple labels
            for ($i = 0; $i < count($a['PackageResults']); $i++) {
                $pkg = $a['PackageResults'][$i];
                $outArr['pkgs'][$i]['pkg_trk_num'] = $pkg['TrackingNumber']['VALUE'];
                $outArr['pkgs'][$i]['label_fmt'] = $pkg['LabelImage']['LabelImageFormat']['Code']['VALUE'];
                $outArr['pkgs'][$i]['label_img'] = $pkg['LabelImage']['GraphicImage']['VALUE'];
            }
        }
        if (array_key_exists('ControlLogReceipt', $a)) {
            $outArr['high_value_report'] = $a['ControlLogReceipt']['GraphicImage']['VALUE'];
        }
        return $outArr;
    }


    /**
     * Simplifies the response array
     *
     * @param array $xmlArray
     * @return array|string
     */
    function simplifyFEDEXResponse($xmlArray)
    {
        $notifications = $xmlArray['v7:ProcessShipmentReply']['v7:Notifications'];

        if (array_key_exists('v7:Code', $notifications)) {
            // If the key exists then get the values
            $code = $notifications['v7:Code']['VALUE'];
            $message = $notifications['v7:Message']['VALUE'];
        } else {
            // If not then its has more siblings, get values from the first one
            $code = $notifications[0]['v7:Code']['VALUE'];
            $message = $notifications[0]['v7:Message']['VALUE'];
        }

        $simpleArr = array();

        if ($code != "0000") {
            return ("Error confirming shipment: " . $code . " (" . $message . ")");
        } else {
            $simpleArr['status'] = 'SUCCESS';
        }


        if (in_array('v7:ShipmentRating', array_keys($xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']))) {
            $simpleArr['charges'] = $xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']['v7:ShipmentRating']['v7:ShipmentRateDetails'][0]['v7:TotalNetFedExCharge']['v7:Amount']['VALUE'];
        }

        $simpleArr['tracking_id'] = $xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']['v7:CompletedPackageDetails']['v7:TrackingIds']['v7:TrackingNumber']['VALUE'];

        if (in_array('v7:CodReturnDetail', array_keys($xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']['v7:CompletedPackageDetails']))) {
            $simpleArr['cod_return_amount'] = $xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']['v7:CompletedPackageDetails']['v7:CodReturnDetail']['v7:CollectionAmount']['v7:Amount']['VALUE'];
        }

        $simpleArr['label_img'] = $xmlArray['v7:ProcessShipmentReply']['v7:CompletedShipmentDetail']['v7:CompletedPackageDetails']['v7:Label']['v7:Parts']['v7:Image']['VALUE'];

        return $simpleArr;
    }

    /**
     * Sends the shipment data to the carrier.
     *
     * After the shipment data is sent it returns a simplified array of
     * the data sent back from the carrier.
     * @return array|bool|string
     */
    function submitShipment()
    {
        switch ($this->carrier) {
            case "UPS":
                $shipResponse = $this->sendUPSshipment();
                $simpleArray = $this->simplifyUPSResponse($shipResponse);
                return $simpleArray;
            case 'USPS':
                return $this->sendUSPSshipment();
            case 'FEDEX':
                $shipResponse = $this->sendFEDEXshipment();
                $simpleArray = $this->simplifyFEDEXResponse($shipResponse);
                return $simpleArray;
            case 'STAMPS':
                return $this->sendSTAMPSshipment();
            default:
                return false;
        }
    }

    function sendShipment()
    {
        switch ($this->carrier) {
            case "UPS":
                return $this->sendUPSshipment();
            default:
                return false;
        }
    }

    private function sendUPSshipment()
    {
        $xml = $this->buildUPSshipmentXML();
        $responseXml = $this->core->request('ShipConfirm', $xml);

        // This is kind of wierd, normally we dont have to reuse the xmlObject in other UPS 
        // services; however, the shipping service requires you to make two seperate XML
        // requests before you get a label.  To prepare for the next XML request (see getLabel)
        // we need to reset the object so nothing is in it.
        $this->core->xmlObject = new \Shipping\Util_XML_Builder(false); // reset the object so getLabel can start a new one

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();

        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();

        return $xmlArray;
    }

    private function sendFEDEXshipment() {

        $xmlString = $this->buildFEDEXShipmentXml();

        // Put the xml that is sent to FedEx into a variable so we can call it later for debugging.
        $this->core->xmlSent = $xmlString;
        $this->core->xmlResponse = $this->core->request($xmlString);

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();
        return $xmlArray;
    }


    // To the end user this will just show the array (or label)
    // In actuality it is doing the final request to UPS approval process.
    // In this function we are approving the shipment in the sendShipment() function.  
    // In other words it is a two step process.
    // TODO: rename this method and create a new one that only displays the label.
    function getLabels()
    {
        switch ($this->carrier) {
            case "UPS":
                return $this->getUPSlabels();
            default:
                return false;
        }
    }


    private function getUPSlabels()
    {

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $responseArray = $xmlParser->xmlParser($this->core->xmlResponse);
        $responseArray = $xmlParser->getData();

        $shipmentDigest = $responseArray['ShipmentConfirmResponse']['ShipmentDigest']['VALUE'];

        $this->core->access(); // populate the ups->xmlObject with access xml

        $xml = $this->core->xmlObject;
        $xml->push('ShipmentAcceptRequest');
        $xml->push('Request');
        $xml->push('TransactionReference');
        $xml->element('CustomerContext', 'guidlikesubstance');
        $xml->element('XpciVersion', '1.0001');
        $xml->pop(); // end TransactionReference
        $xml->element('RequestAction', 'ShipAccept');
        $xml->pop(); // end Request
        $xml->element('ShipmentDigest', $shipmentDigest);
        $xml->pop(); // end ShipmentAcceptRequest

        $xmlString = $xml->getXml();

        // Store previous xmlSent before putting in new one
        $this->core->xmlPrevSent = $this->core->xmlSent;

        // Put the xml that is sent do UPS into a variable so we can call it later for debugging.
        $this->core->xmlSent = $xmlString;

        // Store previous xml response
        $this->core->xmlPrevResponse = $this->core->xmlResponse;

        $this->core->xmlResponse = $this->core->request('ShipAccept', $xmlString);

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();
        return $xmlArray;
    }


    /**
     * Creates random string of alphanumeric characters
     *
     * @return string
     */
    private function generateRandomString()
    {
        $length = 128;
        $characters = '0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = "";

        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, strlen($characters));
            $string .= substr($characters, $index, 1);
        }
        return $string;
    }
}

?>
