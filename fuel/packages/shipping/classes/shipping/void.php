<?php
/**
* Main class for voiding shipments.
*
* This class is a wrapper for use with all carriers to cancel
* shipments.  Valid carriers are: UPS, USPS, and FedEx.
* To create a shipment see {@link Shipping_Shipment}.
*/
namespace Shipping;

class RocketShipVoid {

    var $OKparams;

    function __Construct($carrier,$license='',$username='',$password='') {
        Shipping::rocketshipit_validateCarrier($carrier);
        $carrier = strtoupper($carrier);
        $this->carrier = $carrier;
        $this->OKparams = Shipping::rocketshipit_getOKparams($carrier);
        if ($carrier == "UPS") {
            $this->core = new ups($license, $username, $password); // This class depends on ups

            if ($license != '') {
                $this->core->license = $license;
            }
            if ($username != '') {
                $this->core->username = $username;
            }
            if ($password != '') {
                $this->core->password = $password;
            }
        }
        if ($carrier == 'FEDEX') {
            $this->core = new \Shipping\Carrier_FedEx(); // This class depends on fedex
        }
        if ($carrier == 'STAMPS') {
            $this->core = new stamps();
        }
    }

    /**
     * Void (cancel) a shipment at the shipment level.  I.e. all packages.
     *
     * This will void all packages linked to the ShipmentIdentification
     * number.  Often times this is the first tracking number in a set
     * of packages.
     * @param $shipmentIdentification
     * @return array|bool|string|void
     */
    function voidShipment($shipmentIdentification) {
        switch ($this->carrier) {
            case "UPS":
                $this->shipmentIdentification = $shipmentIdentification;
                return $this->voidUpsShipment();
                $xmlArray = $this->voidUpsPackage();
                $a = $xmlArray['VoidShipmentResponse'];
                $outArr = "";
                if ($a['Response']['ResponseStatusCode']['VALUE'] == "1") {
                    $outArr['result'] =  "voided";
                    $outArr['trackingNumber'] = $a['PackageLevelResults']['TrackingNumber']['VALUE'];
                } else {
                    $outArr['result'] = "fail";
                    $outArr['reason'] = $a['Response']['Error']['ErrorDescription']['VALUE'] .
                                        " (".$a['Response']['Error']['ErrorCode']['VALUE'].")";
                }
                $outArr['xmlArray'] = $xmlArray;
                return $outArr;
            case "FEDEX":
                $this->shipmentIdentification = $shipmentIdentification;
                return $this->voidFedexShipment();
            case "STAMPS":
                $this->shipmentIdentification = $shipmentIdentification;
                return $this->voidStampsShipment();
            default:
                return false;
        }
    }

    /**
    * Void (cancel) a shipment at the package level.  I.e. one package.
    *
    * This will void a single package identified by a specific
    * tracking number.
    */
    function voidPackage($shipmentIdentification, $packageIdentification) {
        switch ($this->carrier) {
            case "UPS":
                $this->shipmentIdentification = $shipmentIdentification;
                $this->packageIdentification = $packageIdentification;
                $xmlArray = $this->voidUpsPackage();
                $a = $xmlArray['VoidShipmentResponse'];
                $outArr = "";
                if ($a['Response']['ResponseStatusCode']['VALUE'] == "1") {
                    $outArr['result'] =  "voided";
                    $outArr['shipmentNumber'] = $shipmentIdentification;
                } else {
                    $outArr['result'] = "fail";
                    $outArr['reason'] = $a['Response']['Error']['ErrorDescription']['VALUE'] .
                                        " (".$a['Response']['Error']['ErrorCode']['VALUE'].")";
                }
                $outArr['xmlArray'] = $xmlArray;
                return $outArr;
            default:
                return false;
        }
    }

    private function voidUpsPackage() {

        $accessXml = $this->core->xmlObject;

        $xml = new \Shipping\Util_XML_Builder(false);

        $xml->push('VoidShipmentRequest');
            $xml->push('Request');
                $xml->element('RequestAction', '1');
            $xml->pop(); // end Request
        $xml->push('ExpandedVoidShipment');
            $xml->element('ShipmentIdentificationNumber', $this->shipmentIdentification);
            if (is_array($this->packageIdentification)) {
                foreach ($this->packageIdentification as $trackingNumber) {
                    $xml->element('TrackingNumber', $trackingNumber);
                }
            } else {
                    $xml->element('TrackingNumber', $this->packageIdentification);
            }
        $xml->pop(); // end ExpandedVoidShipment
        $xml->pop(); // end VoidShipmentRequest

        $xmlString = $accessXml->getXml(). $xml->getXml();

        $this->core->request('Void',$xmlString);

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();


        return $xmlArray;

    }



    private function voidUpsShipment() {
        $accessXml = $this->core->xmlObject;

        $xml = new \Shipping\Util_XML_Builder(false);

        $xml->push('VoidShipmentRequest');
            $xml->push('Request');
                $xml->element('RequestAction', '1');
            $xml->pop(); // end Request
            $xml->element('ShipmentIdentificationNumber', $this->shipmentIdentification);
        $xml->pop(); // end VoidShipmentRequest

        $xmlString = $accessXml->getXml(). $xml->getXml();

        $this->core->request('Void',$xmlString);

        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();


        return $xmlArray;
    }

    private function voidFedexShipment() {
        $xml = $this->core->xmlObject;
        $xml->push('ns:DeleteShipmentRequest',array('xmlns:ns' => 'http://fedex.com/ws/ship/v7', 'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation' => 'http://fedex.com/ws/ship/v7 ShipService v7.xsd'));
        $this->core->xmlObject = $xml;
        $this->core->access();
        $xml = $this->core->xmlObject;

        $xml->push('ns:TransactionDetail');
            $xml->element('ns:CustomerTransactionId','Dleteshipment_POS');
        $xml->pop(); // end TransactionDetail
        $xml->push('ns:Version');
            $xml->element('ns:ServiceId','ship');
            $xml->element('ns:Major','7');
            $xml->element('ns:Intermediate','0');
            $xml->element('ns:Minor','0');
        $xml->pop(); // end Version
        $xml->element('ns:ShipTimestamp',date("c")); // FedEx uses ISO8601 style timestamps
        $xml->push('ns:TrackingId');
            $xml->element('ns:TrackingIdType',$this->trackingIdType); //GROUND,EXPRESS,USPS
            $xml->element('ns:FormId','String');
            $xml->element('ns:TrackingNumber',$this->shipmentIdentification);
        $xml->pop(); // end TrackingId
        $xml->element('ns:DeletionControl','DELETE_ALL_PACKAGES');
        $xml->pop(); // end DeleteShipmentRequest

        $xmlString = $xml->getXml();

        $this->core->request($xmlString);

        // Convert the xmlString to an array
        $xmlParser = new upsxmlParser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();
        return $xmlArray;
    }

    function setParameter($param,$value) {
        $value = Shipping::rocketshipit_getParameter($param, $value, $this->carrier);
        $this->{$param} = $value;
    }
}

?>