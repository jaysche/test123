<?php
/**
* Main class for tracking shipments and packages
*
* This class is a wrapper for use with all carriers to track packages
* Valid carriers are: UPS, USPS, and FedEx.
*/
namespace Shipping;

class RocketShipTrack {

    function __Construct($carrier,$license='',$username='',$password='') {
        Shipping::rocketshipit_validateCarrier($carrier);
        $this->OKparams = Shipping::rocketshipit_getOKparams($carrier);
        $this->carrier = strtoupper($carrier);
        switch ($this->carrier) {
            case "UPS":
                $this->core = new ups($license, $username, $password); // This class depends on ups

                if ($license != '') {
                    $this->core->license = $license;
                }
                if ($username != '') {
                    $this->core->username = $username;
                }
                if ($password != '') {
                    $this->core->password = $password;
                }

                break;
            case "FEDEX":
                $this->core = new fedex();
                $this->setParameter('trackingIdType','');
                break;
            case "USPS":
                $this->core = new usps();
                $this->setParameter('userid','');
                break;
            default:
                exit ("Unknown carrier $carrier in RocketShipTrack.");
        }
    }

    function track($trackingNumber) {
        switch (strtoupper($this->carrier)) {
        case 'UPS':
            $retArr = $this->trackUPS($trackingNumber);
            $a = $retArr['TrackResponse'];
            if ($a['Response']['ResponseStatusCode']['VALUE'] != "1") {
                $this->result = "FAIL";
                $this->reason = $a['Response']['Error']['ErrorDescription']['VALUE'] .
                                    " (".$a['Response']['Error']['ErrorCode']['VALUE'].")";
            } else {
                if (array_key_exists("TrackingNumber",$a['Shipment']['Package'])) {
                    // single package
                    $p = $a['Shipment']['Package'];
                } else {
                    // multi-package
                    $p = $a['Shipment']['Package'][0];
                }
                $this->result = "OK";
                if (array_key_exists("Status",$p['Activity'])) {
                    // just the one
                    $this->status = $p['Activity']['Status']['StatusType']['Description']['VALUE'];
                } else {
                    // multiple activities - grab the most recent
                    $this->status = $p['Activity'][0]['Status']['StatusType']['Description']['VALUE'];
                }
            }
            return $retArr;
        case 'FEDEX':
            return $this->trackFEDEX($trackingNumber);
        case 'USPS':
            return $this->trackUSPS($trackingNumber);
        default:
            exit("Unknown carrier $carrier in RocketShipTrack");
        }
    }

    function trackByReference($referenceNumber) {
        switch (strtoupper($this->carrier)) {
        case 'UPS':
            $this->referenceNumber = $referenceNumber;
            $retArr = $this->trackUPS($referenceNumber);
            return $retArr;
        case 'FEDEX':
            break;
        case 'USPS':
            break;
        default:
            exit("Unknown carrier $this->carrier in RocketShipTrack");
        }
    }


    // Builds xml for tracking and sends the xml string to the ups->request method
    // recieves a response from UPS and outputs an array.
    function trackUPS($trackingNumber){
        $xml = $this->core->xmlObject;

        $xml->push('TrackRequest',array('xml:lang' => 'en-US'));
            $xml->push('Request');
                $xml->push('TransactionReference');
                    $xml->element('CustomerContext', 'RocketShipIt');
                $xml->pop(); // close TransactionReference
                $xml->element('RequestAction', 'Track');
                $xml->element('RequestOption', 'activity');
            $xml->pop(); // close Request
            if (!isset($this->referenceNumber)) {
                $xml->element('TrackingNumber', $trackingNumber);
            } else {
                $xml->element('ShipperNumber', getUPSDefault('accountNumber'));
                $xml->push('ReferenceNumber');
                    $xml->element('Value', $this->referenceNumber);
                $xml->pop(); // close ReferenceNumber
            }
        $xml->pop();

        // Convert xml object to a string
        $xmlString = $xml->getXml();

        // Send the xmlString to UPS and store the resonse in a class variable, xmlResponse.
        $this->core->request('Track', $xmlString);

        // Return response xml as an array
        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();

        return $xmlArray;
    }






    function setparameter($param,$value) {
        $value = Shipping::rocketshipit_getParameter($param, $value, $this->carrier);
        $this->{$param} = $value;
    }

}

?>