<?php

/**
* Main class for producing package objects that are later inserted into a shipment
* @see Shipping_Shipment::addPackageToShipment()
*/
namespace Shipping;

class Packages {

//    var $ups;

    function __construct($carrier,$license='',$username='',$password='') {
        Shipping::rocketshipit_validateCarrier($carrier);

        $carrier = strtoupper($carrier);
        $this->carrier = $carrier;
        $this->OKparams = Shipping::rocketshipit_getOKparams($carrier);

        // Grab defaults package attributes

        if ($carrier == 'UPS') {
            $this->core = new \Shipping\Carrier_UPS($license, $username, $password); // This class depends on ups

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }

            if ($license != '') {
                $this->core->license = $license;
            }
            if ($username != '') {
                $this->core->username = $username;
            }
            if ($password != '') {
                $this->core->password = $password;
            }

        }
        if ($carrier == 'FEDEX') {
            $this->core = new fedex(); // This class depends on fedex
            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }

        if ($carrier == 'USPS') {
            $this->core = new usps(); // This class depends on usps
            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
    }

    function setParameter($param,$value) {
        $value = Shipping::rocketshipit_getParameter($param, $value, $this->carrier);
        $this->{$param} = $value;
    }

}

?>