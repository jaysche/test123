<?php
/**
* Main Rate class for producing rates for various packages/shipments
*
* This class is a wrapper for use with all carriers to produce rates
* Valid carriers are: UPS, USPS, and FedEx.
*/

namespace Shipping;

class RocketShipRate {

    var $OKparams;
    var $packageCount;

    function __construct($carrier,$license='',$username='',$password='') {
        Shipping::rocketshipit_validateCarrier($carrier);
        $carrier = strtoupper($carrier);
        $this->carrier = $carrier;
        $this->OKparams = Shipping::rocketshipit_getOKparams($carrier);
        $this->packageCount = 0;

        // Set up core class and grab carrier-specific defaults that are unique to the current carrier
        if ($carrier == "UPS") {
            $this->core = new \Shipping\Carrier_UPS($license, $username, $password); // This class depends on ups

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }

            if ($license != '') {
                $this->core->license = $license;
            }
            if ($username != '') {
                $this->core->username = $username;
            }
            if ($password != '') {
                $this->core->password = $password;
            }

        }
        if ($carrier == 'USPS') {
            $this->core = new usps(); // This class depends on usps

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
        if ($carrier == 'FEDEX') {
            $this->core = new \Shipping\Carrier_FedEx(); // This class depends on ups

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
        if ($carrier == 'STAMPS') {
            $this->core = new stamps(); // This class depends on stamps

            foreach ($this->OKparams as $param) {
                $this->setParameter($param, '');
            }
        }
    }

    /**
    * Retruns a single rate from the carrier.
    */
    function getRate() {
        switch ($this->carrier) {
            case "UPS":
                return $this->getUPSRate();
            case "USPS":
                return $this->getUSPSRate();
            case "FEDEX":
                return $this->getFEDEXRate();
        }
    }

    /**
    * Retruns all available rates from the carrier.
    */
    function getAllRates() {
        switch ($this->carrier) {
            case "UPS":
                return $this->getAllUPSRates();
            case 'FEDEX':
                return $this->getAllFEDEXRates();
            case 'USPS':
                return $this->getAllUSPSRates();
            case 'STAMPS':
                return $this->getAllSTAMPSRates();
        }
    }

    /**
    * This is a wrapper to create a running package for each carrier.
    *
    * This is used to add packages to a shipment for any carrier.
    * You use the {@link RocketShipPackage} class to create a package
    * object.
    */
    function addPackageToShipment ($packageObj) {
        $this->packageCount++;
        switch ($this->carrier) {
            case "UPS":
                return $this->addPackageToUPSShipment($packageObj);
            case "USPS":
                return $this->addPackageToUSPSShipment($packageObj, $this->isInternational($this->toCountry));
            case 'FEDEX':
                return $this->addPackageToFEDEXShipment($packageObj);
            default:
                return false;
        }
    }

    /**
    * Return a simple rate from carrier.
    */
    function getSimpleRate() {
        switch($this->carrier) {
            case "UPS":
                $upsArray = $this->getUPSRate();
                $status = $upsArray['RatingServiceSelectionResponse']['Response']['ResponseStatusCode']['VALUE'];
                if ($status == '1') {
                    $rate = $upsArray['RatingServiceSelectionResponse']['RatedShipment']['TotalCharges']['MonetaryValue']['VALUE'];
                    return $rate;
                } else {
                    $errorMessage = $upsArray['RatingServiceSelectionResponse']['Response']['Error']['ErrorDescription']['VALUE'];
                    return $errorMessage;
                }
            case "FEDEX":
                $fedex = $this->getFEDEXRate();
                if(isset($fedex['v7:RateReply']['v7:HighestSeverity']['VALUE']) && $fedex['v7:RateReply']['v7:HighestSeverity']['VALUE'] != 'ERROR') {
                    return $fedex['v7:RateReply']['v7:RateReplyDetails']['v7:RatedShipmentDetails'][1]['v7:ShipmentRateDetail']['v7:TotalNetCharge']['v7:Amount']['VALUE'];
                } else {
                    return $fedex['v7:RateReply']['v7:Notifications']['v7:Message']['VALUE'];
                }
                return $fedex;
            case "USPS":
                $usps = $this->getUSPSRate();
                if(!isset($usps['Error']) && !isset($usps['RateV4Response']['Package']['Error'])) {
                    return $usps['RateV4Response']['Package']['Postage']['Rate']['VALUE'];
                } else {
                    if (isset($usps['Error']['Description']['VALUE'])) {
                        return $usps['Error']['Description']['VALUE'];
                    } else {
                        return $usps['RateV4Response']['Package']['Error']['Description']['VALUE'];
                    }
                }
        }
    }

    /**
    * Return all available rates from carrier in a simple array.
    */
    function getSimpleRates() {
        switch($this->carrier) {
            case "UPS":
                $upsArray = $this->getAllUPSRates();
                $status = $upsArray['RatingServiceSelectionResponse']['Response']['ResponseStatusCode']['VALUE'];

                if ($status == '1') {
                    $rate = $upsArray['RatingServiceSelectionResponse']['RatedShipment'][0]['TotalCharges']['MonetaryValue']['VALUE'];
                    $service = $upsArray['RatingServiceSelectionResponse']['RatedShipment'];

                    $rates = Array();
                    if (array_key_exists('Service', $service)) {
                        $r = $service['Service']['Code']['VALUE'];
                        $desc = $this->core->getServiceDescriptionFromCode($r);
                        $rates["$desc"] = $service['TotalCharges']['MonetaryValue']['VALUE'];
                    } else {
                        foreach ($service as $s) {
                            $r = $s['Service']['Code']['VALUE'];
                            $desc = $this->core->getServiceDescriptionFromCode($r);
                            $rates["$desc"] = $s['TotalCharges']['MonetaryValue']['VALUE'];
                            if(isset($s['NegotiatedRates'])) {
                                $rates["$desc"] = array(
                                	'Rate' => $s['TotalCharges']['MonetaryValue']['VALUE'],
                                    'Negotiated' => $s['NegotiatedRates']['NetSummaryCharges']['GrandTotal']['MonetaryValue']['VALUE']
                                );
                            }
                        }
                    }

                    return $rates;
                } else {
                    $errorMessage = $upsArray['RatingServiceSelectionResponse']['Response']['Error']['ErrorDescription']['VALUE'];
                    $errorArray['error'] = $errorMessage;
                    return $errorArray;
                }
            case 'FEDEX':
                $fedex = $this->getAllFEDEXRates();
                if(isset($fedex['v7:RateReply']['v7:HighestSeverity']['VALUE']) && $fedex['v7:RateReply']['v7:HighestSeverity']['VALUE'] == 'SUCCESS') {
                    $service = $fedex['v7:RateReply']['v7:RateReplyDetails'];

                    $rates = Array();
                    foreach ($service as $s) {
                        $serviceType = $s['v7:ServiceType']['VALUE'];
                        if (isset($s['v7:RatedShipmentDetails'][0])) {
                            $value = $s['v7:RatedShipmentDetails'][0]['v7:ShipmentRateDetail']['v7:TotalNetCharge']['v7:Amount']['VALUE'];
                        } else {
                            $value = $s['v7:RatedShipmentDetails']['v7:ShipmentRateDetail']['v7:TotalNetCharge']['v7:Amount']['VALUE'];
                        }
                        $rates["$serviceType"] = $value;
                    }
                    return $rates;
                } else {
                    $errorMessage = $fedex['v7:RateReply']['v7:Notifications']['v7:Message']['VALUE'];
                    $errorArray['error'] = $errorMessage;
                    return $errorArray;
                }
            case 'USPS':
                $usps = $this->getAllUSPSRates();
                if(!isset($usps['Error']) && !isset($usps['RateV4Response']['Package']['Error'])) {
                    if (array_key_exists('RateV4Response', $usps)) {
                        $services = $usps['RateV4Response']['Package']['Postage'];
                        $rates = Array();
                        if (isset($services[0])) {
                            foreach ($services as $s) {
                                $service = $s['MailService']['VALUE'];
                                $value = $s['Rate']['VALUE'];
                                $rates["$service"] = $value;
                            }
                        } else {
                            $service = $services['MailService']['VALUE'];
                            $value = $services['Rate']['VALUE'];
                            $rates["$service"] = $value;
                        }
                        return $rates;
                    } else {
                        $services = $usps['IntlRateResponse']['Package']['Service'];
                        $rates = Array();
                        foreach ($services as $s) {
                            $service = $s['SvcDescription']['VALUE'];
                            $value = $s['Postage']['VALUE'];
                            $rates["$service"] = $value;
                        }
                        return $rates;
                    }
                } else {
                    if (isset($usps['Error']['Description']['VALUE'])) {
                        $errorMessage = $usps['Error']['Description']['VALUE'];
                        $errorArray['error'] = $errorMessage;
                        return $errorArray;
                    } else {
                        $errorMessage = $usps['RateV4Response']['Package']['Error']['Description']['VALUE'];
                        $errorArray['error'] = $errorMessage;
                        return $errorArray;
                    }
                }
            case 'STAMPS':
                $stamps = $this->getAllSTAMPSRates();
                // TODO: If error do an array with error as key and description as value
                // else, for each rate find the description and value and put it into an array
                $rates = Array();
                foreach ($stamps->Rates->Rate as $rte) {
                    if ($rte) {
                        $svc_code = $rte->ServiceType;
                        $service = $this->core->getServiceDescriptionFromCode($svc_code);
                        $value = $rte->Amount;
                        $packageType = $rte->PackageType;
                        $rates["$service - $packageType"] = $value;
                    }
                }
                return $rates;
        }
    }

    private function getAllUPSRates() {
        return $this->getUPSRate('Shop');
    }

    private function getAllFEDEXRates() {
        return $this->getFEDEXRate(true);
    }

    private function getAllUSPSRates() {
        return $this->getUSPSRate(true);
    }


    private function getUPSRate($requestOption='Rate') {

        $xmlString = $this->buildUPSRateXml($requestOption);

        $this->core->request('Rate', $xmlString);

        // Convert the xmlString to an array
        $xmlParser = new \Shipping\Util_UPS_XML_Parser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();
        return $xmlArray;
    }

    private function getFEDEXRate($allAvailableRates=false) {
        $xmlString = $this->buildFEDEXRateXml($allAvailableRates);

        $this->core->request($xmlString);

        // Convert the xmlString to an array
        $xmlParser = new upsxmlParser();
        $xmlArray = $xmlParser->xmlparser($this->core->xmlResponse);
        $xmlArray = $xmlParser->getData();
        return $xmlArray;
    }

    function addPackageToUPSShipment($package) {

        if (!isset($this->core->packagesObject)) {
            $this->core->packagesObject = new \Shipping\Util_XML_Builder(true);
        }

        $xml = $this->core->packagesObject;

        $xml->push('Package');
            $xml->push('PackagingType');
                $xml->element('Code', $package->packagingType);
                //$xml->element('Description', $this->packageTypeDescription);
            $xml->pop(); // close PacakgeType
                if ($package->length != '') {
                    $xml->push('Dimensions');
                        $xml->push('UnitOfMeasurement');
                            $xml->element('Code', $package->lengthUnit);
                        $xml->pop(); // close UnitOfMeasurement
                            $xml->element('Length', $package->length);
                            $xml->element('Width', $package->width);
                            $xml->element('Height', $package->height);
                    $xml->pop(); // close Dimensions
                }
            //$xml->element('Description', $this->packageDescription);
            $xml->push('PackageWeight');
                $xml->push('UnitOfMeasurement');
                    $xml->element('Code', $package->weightUnit);
                $xml->pop(); // close UnitOfMeasurement
                $xml->element('Weight', $package->weight);
            $xml->pop(); // close PackageWeight
            if ($package->monetaryValue != '' || $package->insuredCurrency != '' || $package->signatureType != '') {// Change for COD
                $xml->push('PackageServiceOptions');
                    if ($package->monetaryValue != '') {
                        $xml->push('InsuredValue');
                            $xml->element('CurrencyCode',$package->insuredCurrency);
                            $xml->element('MonetaryValue',$package->monetaryValue);
                        $xml->pop(); // close InsuredValue
                    }
                    if ($package->signatureType != '') {
                        $xml->push('DeliveryConfirmation');
                            $xml->element('DCISType', $package->signatureType);
                        $xml->pop(); // end DeliveryConfirmation
                    }
                $xml->pop(); // close PackageServiceOptions
            }
        $xml->pop(); // close Package

        $this->core->packagesObject = $xml;

        return true;
    }

    function addPackageToFEDEXShipment($package) {

        if (!isset($this->core->packagesObject)) {
            $this->core->packagesObject = new xmlBuilder(true);
        }

        $xml = $this->core->packagesObject;

        $xml->push('ns:RequestedPackageLineItems');
            $xml->element('ns:SequenceNumber','1');
            if ($this->insuredValue != '' && $this->insuredCurrency != '') {
                $xml->push('ns:InsuredValue');
                    $xml->element('ns:Currency', $this->insuredCurrency);
                    $xml->element('ns:Amount', $this->insuredValue);
                $xml->pop(); // end InsuredValue
            }
            $xml->push('ns:Weight');
                $xml->element('ns:Units',$package->weightUnit);
                $xml->element('ns:Value',$package->weight);
            $xml->pop(); // end Weight
            if ($package->length != '') {
                $xml->push('ns:Dimensions');
                    if ($package->length != '') {
                        $xml->element('ns:Length',$package->length);
                    }
                    if ($package->width != '') {
                        $xml->element('ns:Width',$package->width);
                    }
                    if ($package->height != '') {
                        $xml->element('ns:Height',$package->height);
                    }
                    $xml->element('ns:Units',$package->lengthUnit);
                $xml->pop(); // end Dimensions
            }
            if ($this->signatureType != '') {
                $xml->push('ns:SpecialServicesRequested');
                    $xml->element('ns:SpecialServiceTypes', 'SIGNATURE_OPTION');
                    $xml->push('ns:SignatureOptionDetail');
                        $xml->element('ns:OptionType', $this->signatureType);
                    $xml->pop();
                $xml->pop(); // end ShipmentSpecialServicesRequested
            }
        $xml->pop(); // end RequestedPackageLineItems

        $this->core->packagesObject = $xml;
        $this->packageCount++;
        return true;
    }

    function buildUPSRateXml($requestOption='Rate') {
        $xml = $this->core->xmlObject;

        $xml->push('RatingServiceSelectionRequest');
            $xml->push('Request');
                $xml->element('RequestAction', 'Rate');
                $xml->element('RequestOption', $requestOption);
                $xml->push('TransactionReference'); // Not required
                    $xml->element('CustomerContext', 'RocketShipIt'); // Not required
                    //$xml->element('XpciVersion', '1.0'); // Not required
                $xml->pop(); // close TransactionReference, not required
            $xml->pop(); // close Request
            $xml->push('PickupType');
                $xml->element('Code', $this->PickupType); // TODO: insert link to code values
                if ($this->pickupDescription != '') {
                    //$xml->element('Description', $this->pickupDescription);
                }
            $xml->pop(); // close PickupType
            if ($this->customerClassification != '') {
                $xml->push('CustomerClassification');
                    $xml->element('Code', $this->customerClassification);
                $xml->pop(); //end CustomerClassification
            }
            $xml->push('Shipment');
                //$xml->element('Description', $this->shipmentDescription);
                $xml->push('Shipper');
                    $xml->element('ShipperNumber', $this->accountNumber);
                    $xml->push('Address');
                        $xml->element('AddressLine1', $this->shipAddr1);
                        if ($this->shipAddr2 != '') {
                            $xml->element('AddressLine2', $this->shipAddr2);
                        }
                        if ($this->shipAddr3 != '') {
                            $xml->element('AddressLine3', $this->shipAddr3);
                        }
                        if ($this->shipCity != '') {
                            $xml->element('City', $this->shipCity);
                        }
                        $xml->element('StateProvinceCode', $this->shipState);
                        $xml->element('PostalCode', $this->shipCode);
                        if ($this->shipCountry != '') {
                            $xml->element('CountryCode', $this->shipCountry);
                        } else {
                            $xml->element('CountryCode', 'US');
                        }
                    $xml->pop(); // close Address
                $xml->pop(); // close Shipper
                $xml->push('ShipTo');
                    if ($this->toCompany != '') {
                        $xml->element('CompanyName', $this->toCompany);
                    }
                    $xml->push('Address');
                        if ($this->toAddr1 != '') {
                            $xml->element('AddressLine1', $this->toAddr1);
                        }
                        if ($this->toAddr2 != '') {
                            $xml->element('AddressLine2', $this->toAddr2);
                        }
                        if ($this->toAddr3 != '') {
                            $xml->element('AddressLine3', $this->toAddr3);
                        }
                        if ($this->toCity != '') {
                            $xml->element('City', $this->toCity);
                        }
                        if ($this->toState != '') {
                            $xml->element('StateProvinceCode', $this->toState);
                        }
                        $xml->element('PostalCode', $this->toCode);
                        if ($this->toCountry != '') {
                            $xml->element('CountryCode', $this->toCountry);
                        } else {
                            $xml->element('CountryCode', 'US');
                        }
                        if ($this->residentialAddressIndicator != '') {
                            $xml->element('ResidentialAddressIndicator', '1');
                        }
                    $xml->pop(); // close Address
                $xml->pop(); // close ShipTo
            if ($this->fromName != '') {
                $xml->push('ShipFrom');
                    $xml->element('CompanyName', $this->fromName);
                    //$xml->element('AttentionName', $this->fromAttentionName);
                    //$xml->element('PhoneNumber', $this->fromPhoneNumber);
                    //$xml->element('FaxNumber', $this->fromFaxNumber);
                    $xml->push('Address');
                        $xml->element('AddressLine1', $this->fromAddr1);
                        $xml->element('AddressLine2', $this->fromAddr2);
                        $xml->element('AddressLine3', $this->fromAddr3);
                        $xml->element('City', $this->fromCity);
                        $xml->element('PostalCode', $this->fromCode);
                        if ($this->fromCountry != '') {
                            $xml->element('CountryCode', $this->fromCountry);
                        } else {
                            $xml->element('CountryCode', 'US');
                        }
                    $xml->pop(); // close Address
                $xml->pop(); // close ShipFrom
            }
            if ($this->service != '') {
                $xml->push('Service');
                    $xml->element('Code', $this->service);
                $xml->pop(); // close Service
            }
            if (!isset($this->core->packagesObject)) {
                $xml->push('Package');
                    $xml->push('PackagingType');
                        $xml->element('Code', $this->packagingType);
                        //$xml->element('Description', $this->packageTypeDescription);
                    $xml->pop(); // close PacakgeType
                        if ($this->length != '' && $this->width != '' && $this->height != '') {
                            $xml->push('Dimensions');
                                $xml->push('UnitOfMeasurement');
                                    $xml->element('Code', $this->lengthUnit);
                                $xml->pop(); // close UnitOfMeasurement
                                    $xml->element('Length', $this->length);
                                    $xml->element('Width', $this->width);
                                    $xml->element('Height', $this->height);
                            $xml->pop(); // close Dimensions
                        }
                    //$xml->element('Description', $this->packageDescription);
                    if (isset($this->weightUnit)) {
                        $xml->push('PackageWeight');
                            $xml->push('UnitOfMeasurement');
                                $xml->element('Code', $this->weightUnit);
                            $xml->pop(); // close UnitOfMeasurement
                            if ($this->weight != '') {
                                $xml->element('Weight', $this->weight);
                            } else {
                                $xml->element('Weight', '0');
                            }
                        $xml->pop(); // close PackageWeight
                    }
                    if ($this->monetaryValue != '' || $this->insuredCurrency != '' || $this->signatureType != '') {// Change for COD
                        $xml->push('PackageServiceOptions');
                            if ($this->monetaryValue != '') {
                                $xml->push('InsuredValue');
                                    $xml->element('CurrencyCode', $this->insuredCurrency);
                                    $xml->element('MonetaryValue', $this->monetaryValue);
                                $xml->pop(); // close InsuredValue
                            }
                            if ($this->signatureType != '') {
                                $xml->push('DeliveryConfirmation');
                                    $xml->element('DCISType', $this->signatureType);
                                $xml->pop(); // end DeliveryConfirmation
                            }
                        $xml->pop(); // close PackageServiceOptions
                    }
                $xml->pop(); // close Package
            } else {
                $xmlString = $xml->getXml();
                $xmlString .= $this->core->packagesObject->getXml();
                $xmlString .= '</Shipment>'."
";
                $xmlString .= '</RatingServiceSelectionRequest>'."
";
                return $xmlString;
            }
            if ($this->negotiatedRates == '1') {
                $xml->push('RateInformation');
                    $xml->element('NegotiatedRatesIndicator','1');
                $xml->pop(); // close RateInformation
            }
            //$xml->push('ShipmentServiceOptions');
            //$xml->pop(); // close ShipmentServiceOptions
            $xml->pop(); // close Shipment
        $xml->pop();

        // Convert xml object to a string
        $xmlString = $xml->getXml();
        return $xmlString;
    }

    function buildFEDEXRateXml($allAvailableRates=false) {
        $xml = $this->core->xmlObject;
        $xml->push('ns:RateRequest',array('xmlns:ns' => 'http://fedex.com/ws/rate/v7', 'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation' => 'http://fedex.com/ws/rate/v7 RateService v7.xsd'));

        $this->core->xmlObject = $xml;
        $this->core->access();
        $xml = $this->core->xmlObject;

        $xml->push('ns:Version');
            $xml->element('ns:ServiceId','crs');
            $xml->element('ns:Major','7');
        $xml->pop(); // end Version
        $xml->push('ns:RequestedShipment');
            if (!$allAvailableRates) {
                $xml->element('ns:ServiceType',$this->service);
            }
            $xml->element('ns:PackagingType',$this->packagingType);
            $xml->push('ns:Shipper');
                $xml->push('ns:Address');
                    $xml->element('ns:StreetLines',$this->shipAddr1);
                    $xml->element('ns:City',$this->shipCity);
                    $xml->element('ns:StateOrProvinceCode',$this->shipState);
                    $xml->element('ns:PostalCode',$this->shipCode);
                    $xml->element('ns:CountryCode',$this->shipCountry);
                $xml->pop(); // end Address
            $xml->pop(); // end Shipper
            $xml->push('ns:Recipient');
                $xml->element('ns:AccountNumber','ACCOUNT');
                if ($this->toName !='' || $this->toCompany != '') {
                    $xml->push('ns:Contact');
                        if ($this->toName != '') {
                            $xml->element('ns:PersonName',$this->toName);
                        }
                        if ($this->toCompany != '') {
                            $xml->element('ns:CompanyName',$this->toCompany);
                        }
                    $xml->pop(); // end Contact
                }
                $xml->push('ns:Address');
                    $xml->element('ns:PostalCode',$this->toCode);
                    $xml->element('ns:CountryCode',$this->toCountry);
                    if ($this->residential != '') {
                        $xml->element('ns:Residential',$this->residential);
                    }
                $xml->pop(); // end Address
            $xml->pop(); // end Recipient
            if ($this->saturdayDelivery == 'YES') {
                $xml->push('ns:SpecialServicesRequested');
                    $xml->element('ns:SpecialServiceTypes', 'SATURDAY_DELIVERY');
                $xml->pop(); // end ShipmentSpecialServicesRequested
            }
            $xml->element('ns:RateRequestTypes', 'LIST');
            $xml->element('ns:PackageCount', ($this->packageCount == 0) ? '1' : $this->packageCount);
            $xml->element('ns:PackageDetail','INDIVIDUAL_PACKAGES');
            if (!isset($this->core->packagesObject)) {
                $xml->push('ns:RequestedPackageLineItems');
                    $xml->element('ns:SequenceNumber','1');
                    if ($this->insuredValue != '' && $this->insuredCurrency != '') {
                        $xml->push('ns:InsuredValue');
                            $xml->element('ns:Currency', $this->insuredCurrency);
                            $xml->element('ns:Amount', $this->insuredValue);
                        $xml->pop(); // end InsuredValue
                    }
                    $xml->push('ns:Weight');
                        $xml->element('ns:Units',$this->weightUnit);
                        $xml->element('ns:Value',$this->weight);
                    $xml->pop(); // end Weight
                    if ($this->length != '') {
                        $xml->push('ns:Dimensions');
                            if ($this->length != '') {
                                $xml->element('ns:Length',$this->length);
                            }
                            if ($this->width != '') {
                                $xml->element('ns:Width',$this->width);
                            }
                            if ($this->height != '') {
                                $xml->element('ns:Height',$this->height);
                            }
                            $xml->element('ns:Units',$this->lengthUnit);
                        $xml->pop(); // end Dimensions
                    }
                    if ($this->signatureType != '') {
                        $xml->push('ns:SpecialServicesRequested');
                            $xml->element('ns:SpecialServiceTypes', 'SIGNATURE_OPTION');
                            $xml->push('ns:SignatureOptionDetail');
                                $xml->element('ns:OptionType', $this->signatureType);
                            $xml->pop();
                        $xml->pop(); // end ShipmentSpecialServicesRequested
                    }
                $xml->pop(); // end RequestedPackageLineItems
            } else {
                $xmlString = $xml->getXml();
                $xmlString .= $this->core->packagesObject->getXml();
                $xmlString .= '</ns:RequestedShipment>'."
";
                $xmlString .= '</ns:RateRequest>'."
";
                return $xmlString;
            }
        $xml->pop(); // end RequestedShipment
        $xml->pop(); // end RateRequest

        $xmlString = $xml->getXml();
        return $xmlString;
    }

    // In order to allow users to override defaults or specify obscure UPS
    // data, this function allows you to set any of the variables that this class uses
    function setParameter($param,$value) {
        $value = Shipping::rocketshipit_getParameter($param, $value, $this->carrier);
        $this->{$param} = $value;
    }

    // Checks the country to see if the request is International
    function isInternational($country) {
        if ($country == '' || $country == 'US' || $country == $this->core->getCountryName('US')) {
            return false;
        }
        return true;
    }
}

?>