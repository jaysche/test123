<?php

namespace Shipping;

/**
 * Core FedEx Class
 *
 * Used internally to send data, set debug information, change
 * urls, and build xml
 */
class Carrier_FedEx
{

    function __construct()
    {
        // Grab the fedex key, username and password for defaults
        $this->fedexkey = \Config::get('fedex.key');
        $this->password = \Config::get('fedex.password');
        $this->accountNumber = \Config::get('fedex.accountNumber');
        $this->meterNumber = \Config::get('fedex.meterNumber');

        $this->debugMode = \Config::get('generic.debugMode');
        $this->setTestingMode($this->debugMode);

        // Create a new xmlObject to be used by access and other classes
        // This object will be used all the way through, until the final xmlObject
        // is converted to a string just before sending to UPS
        $this->xmlObject = new \Shipping\Util_XML_Builder(true);
    }

    // Create the auth xml that is used in every request
    function access() {
        $xml = $this->xmlObject;
        $xml->push('ns:WebAuthenticationDetail');
            $xml->push('ns:UserCredential');
                $xml->element('ns:Key',$this->fedexkey);
                $xml->element('ns:Password',$this->password);
            $xml->pop(); // end UserCredential
        $xml->pop(); // end WebAuthenticationDetail
        $xml->push('ns:ClientDetail');
            $xml->element('ns:AccountNumber',$this->accountNumber);
            $xml->element('ns:MeterNumber',$this->meterNumber);
        $xml->pop(); // end ClientDetail

        $this->xmlObject = $xml;

        $this->accessRequest = true;
        return $this->xmlObject->getXml(); // returns xmlstring, but probably not used
    }

    // This function is the only function that actually transmits and recieves data
    // from FedEx. All classes use this to send XML to FedEx servers.
    function request($xml, $isMultiRequest=false){
        $this->xmlSent = $xml; // Store the xmlSent for debugging
        $output = preg_replace('/[\s+]{2,}/', '', $xml);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->fedexUrl.'/xml');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT, 60); // Request timeout
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $output);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if ($isMultiRequest) {
            return $ch;
        }
        $curlReturned = curl_exec($ch);
        curl_close($ch);
        //exit ($curlReturned);
        $this->curlReturned = $curlReturned; // Store curl response for debugging

        // Find out if the UPS service is down
        preg_match_all('/HTTP\/1\.\d\s(\d+)/',$curlReturned,$matches);
        foreach($matches[1] as $key=>$value) {
            if ($value != 100 && $value != 200) {
                throw new RuntimeException("The FedEx service seems to be down with HTTP/1.1 $value");
            } else {
                $response = strstr($curlReturned, '<?'); // Separate the html header and the actual XML because we turned CURLOPT_HEADER to 1
                $this->xmlResponse = $response;
                return $response;
            }
        }
    }

    // This function checks the value of debugMode and changes the FedEx url accordingly.  This is because
    // FedEx has a testing and production server.
    function setTestingMode($bool){
        if($bool == 1){
            $this->debugMode = true;
            $this->fedexUrl = 'https://gatewaybeta.fedex.com'; // Don't put a trailing slash here or world will collide.
        } else {
            $this->debugMode = false;
            $this->fedexUrl = 'https://gateway.fedex.com';
        }
        return true;
    }

    // Function to provide vital enviornment and request/response debugging details
    function debug() {
        $debugInfo = '<pre>';
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= 'Debug Information'. "
";
        $debugInfo .= '--------------------------------------------------'. "
";
        if (isset($this->debugMode)) {
            $debugInfo .= "debugMode = $this->debugMode";
        }
        $debugInfo .= "

";
        if (isset($this->xmlPrevSent)) {
            $debugInfo .= '--------------------------------------------------'. "
";
            $debugInfo .= 'XML Prev Sent'. "
";
            $debugInfo .= '--------------------------------------------------'. "
";
            $debugInfo .= htmlentities($this->xmlPrevSent) . "
";
            $debugInfo .= "

";
        }
        if (isset($this->xmlPrevResponse)) {
            $debugInfo .= '--------------------------------------------------'. "
";
            $debugInfo .= 'XML Prev Response'. "
";
            $debugInfo .= '--------------------------------------------------'. "
";
            $debugInfo .= htmlentities($this->xmlPrevResponse) . "
";
            $debugInfo .= "

";
        }
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= 'XML Sent'. "
";
        $debugInfo .= '--------------------------------------------------'. "
";
        if (isset($this->xmlSent)) {
            $debugInfo .= htmlentities($this->xmlSent) . "
";
        } else {
            $debugInfo .= 'xmlSent was not set'. "
";
        }
        $debugInfo .= "

";
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= 'XML Response'. "
";
        $debugInfo .= '--------------------------------------------------'. "
";
        if (isset($this->xmlResponse)) {
            $debugInfo .= htmlentities($this->xmlResponse) . "
";
        } else {
            $debugInfo .= 'xmlResponse was not set'. "
";
        }
        $debugInfo .= "

";
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= 'PHP Information'. "
";
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= phpversion();
        $debugInfo .= "

";
        $debugInfo .= '--------------------------------------------------'. "
";
        $debugInfo .= 'cURL Return Information'. "
";
        $debugInfo .= '--------------------------------------------------'. "
";
        if (isset($this->curlReturned)) {
            $debugInfo .= htmlentities($this->curlReturned);
        }
        $debugInfo .= "

";
        $debugInfo .= '</pre>';
        return $debugInfo;
    }
}

?>
