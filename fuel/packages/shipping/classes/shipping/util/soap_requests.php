<?php
namespace Shipping;

// Main class for sending soap requests to the pickup API.
// This is not done in XML because UPS doesn't support XML
// for pickup requests.
class SoapRequests
{
    function upsSoapRequest($request, $methodCode) {

        // Get credential information from config
        $license = \Config::get('ups.license');
        $username = \Config::get('ups.username');
        $password = \Config::get('ups.password');

        if (\Config::get('generic.debugMode') == 1) {
            $soapUrl = "https://wwwcie.ups.com/webservices/Pickup";
        } else {
            $soapUrl = "https://onlinetools.ups.com/webservices/Pickup";
        }

        $client = new SoapClient(__DIR__ . "/schemas/UPSPickup/Pickup.wsdl", array('trace' => 1, "location" => $soapUrl));
        $ns = "http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0";
        $security = array("UsernameToken" => array("Username" => $username, "Password" => $password), "ServiceAccessToken" => array("AccessLicenseNumber" => $license));
        $header = new SOAPHeader($ns, "UPSSecurity", $security);
        $client->__setSoapHeaders($header);

        //var_dump($client->__getFunctions());die();

        $methods = array(
            0 => 'ProcessPickupCreation',
            1 => 'ProcessPickupRate',
            2 => 'ProcessPickupCancel',
            3 => 'ProcessPickupPendingStatus',
        );

        try {
            $response = call_user_func_array(array($client, $methods[$methodCode]), array($request));
            //$response = $client->PessPickupRate($request);
        } catch (exception $ex) {
            echo $ex->faultstring;
            print_r($ex->detail);
            //echo $client->__getLastRequest();
            //var_dump($ex->faultcode, $ex->faultstring, $ex->detail);
            //echo $fault->getMessage();
            //echo $fault->faultstring();
            //echo $client->__getLastResponse();
        }
        //echo $client->__getLastRequest();
        return $response;
    }
}