<?php

namespace Shipping;

// Simon Willison, 16th April 2003
// Based on Lars Marius Garshol's Python XMLWriter class
// See http://www.xml.com/pub/a/2003/04/09/py-xml.html

// modified 2009-07-09 RCE
//  added $subordinateSection parameter; if true, class does not place "xml version" at the start

/**
 * Class used to build xml internally
 */
class Util_XML_Builder
{

    var $xml;
    var $indent;
    var $stack = array();

    public static function _init() {
        \Config::load('generic', true);
    }

    function xmlBuilder($subordinateSection = false)
    {
        $this->indent = \Config::get('generic.debugMode') == 1 ? '   ' : ''; // indent if debugging
        if (!$subordinateSection) {
            $this->xml = '<?xml version="1.0"?>' . "\n";
        }
    }

    function _indent()
    {
        for ($i = 0, $j = count($this->stack); $i < $j; $i++) {
            $this->xml .= $this->indent;
        }
    }

    function push($element, $attributes = array())
    {
        $this->_indent();
        $this->xml .= '<' . $element;
        foreach ($attributes as $key => $value) {
            $this->xml .= ' ' . $key . '="' . htmlentities($value, ENT_NOQUOTES, 'ISO-8859-1', false) . '"';
        }
        $this->xml .= ">\n";
        $this->stack[] = $element;
    }

    function element($element, $content, $attributes = array())
    {
        if ($content != '') {
            $this->_indent();
            $this->xml .= '<' . $element;
            foreach ($attributes as $key => $value) {
                $this->xml .= ' ' . $key . '="' . htmlentities($value, ENT_NOQUOTES, 'ISO-8859-1', false) . '"';
            }
            $this->xml .= '>' . htmlentities($content, ENT_NOQUOTES, 'ISO-8859-1', false) . '</' . $element . '>' . "\n";
        }
    }

    function emptyelement($element, $attributes = array())
    {
        $this->_indent();
        $this->xml .= '<' . $element;
        foreach ($attributes as $key => $value) {
            $this->xml .= ' ' . $key . '="' . htmlentities($value, ENT_NOQUOTES, 'ISO-8859-1', false) . '"';
        }
        $this->xml .= " />\n";
    }

    function pop()
    {
        $element = array_pop($this->stack);
        $this->_indent();
        $this->xml .= "</$element>\n";
    }

    // Added MS 03-31-2011
    function append($xmlString)
    {
        $this->xml .= "$xmlString";
    }

    function getXml()
    {
        return $this->xml;
    }
}
?>
