<?php

namespace Shipping;

/**
 * Shipping
 *
 * @package     Fuel
 * @subpackage  Shipping
 */
class Shipping
{
    /*
     * Loads the configuration files so we can grab our properties
     * TODO: Filter our which services are loaded so we don't have to autoload all config files
     */
    public static function _init() {
        \Config::load('generic', true);
        \Config::load('ups', true);
    }

    /**
     * Prevent instantiation
     */
    final private function __construct()
    {
        // Error if cURL is not present
        if (!extension_loaded('curl')) {
            exit('The required php extension, cURL, was not found.  Please install the cURL module to continue use the shipping package.');
        }
    }

    /**
     * @static
     * @param null $instance
     * @return bool|null
     */
	public static function instance($instance = null)
	{
		if ($instance !== null)
		{
			if ( ! array_key_exists($instance, static::$_instances))
			{
				return false;
			}

			return static::$_instances[$instance];
		}

		if (static::$_instance === null)
		{
			static::$_instance = static::factory();
		}

		return static::$_instance;
	}


    /**
     * * Ensures that only settable paramaters are allowed.
     *
     * This function aids the setPramater() function in that it only
     * allows known paramaters to be set.  This helps to avoid typos when
     * setting parameters.
     *
     * @throws RuntimeException
     * @param $carrier
     * @return array
     */
    public static function rocketshipit_getOKparams($carrier)
    {
        // Force fedex, FedEx, FEDEX to all read the same
        $carrier = strtoupper($carrier);

        // Generic parameters that are accessible in each class regardless of carrier
        $generic = array('shipper', 'enableRocketShipAPI', 'shipContact', 'shipPhone',
                         'accountNumber', 'shipAddr1', 'shipAddr2', 'shipAddr3',
                         'shipCity', 'shipState', 'shipCode', 'shipCountry',
                         'toCompany', 'toName', 'toPhone', 'toAddr1', 'toAddr2',
                         'toAddr3', 'toCity', 'toState', 'toCountry', 'toCode',
                         'service', 'weightUnit', 'length', 'width', 'height',
                         'weight', 'toExtendedCode', 'currency');

        // Carrier specific parameters
        switch ($carrier) {
            case "UPS":
                $specific = array('earliestTimeReady', 'latestTimeReady',
                                  'httpUserAgent', 'labelPrintMethodCode',
                                  'labelDescription', 'labelHeight', 'labelWidth',
                                  'labelImageFormat', 'residentialAddressIndicator',
                                  'PickupType', 'pickupDescription',
                                  'shipmentDescription', 'packagingType',
                                  'packageLength', 'packageWidth', 'packageHeight',
                                  'packageWeight', 'referenceCode', 'referenceValue',
                                  'insuredCurrency', 'monetaryValue',
                                  'referenceCode2', 'referenceValue2', 'pickupDate',
                                  'lengthUnit', 'serviceDescription', 'returnCode',
                                  'fromName', 'fromAddr1', 'fromAddr2', 'fromAddr3',
                                  'fromCity', 'fromState', 'fromCode', 'fromCountry',
                                  'fromAttentionName', 'fromPhoneNumber', 'fromFaxNumber',
                                  'packageDescription', 'returnEmailAddress',
                                  'returnUndeliverableEmailAddress',
                                  'returnFromEmailAddress', 'returnEmailFromName',
                                  'verifyAddress', 'negotiatedRates',
                                  'saturdayDelivery', 'billThirdParty',
                                  'thirdPartyAccount', 'codFundType',
                                  'codAmount', 'flexibleAccess', 'signatureType',
                                  'customerClassification', 'pickupAddr1',
                                  'pickupCity', 'pickupState', 'pickupCode',
                                  'pickupCountry', 'pickupResidential', 'pickupAlternative',
                                  'closeTime', 'readyTime', 'pickupCompanyName', 'pickupContactName',
                                  'pickupPhone', 'pickupServiceCode', 'pickupQuantity', 'pickupDestination',
                                  'pickupContainerCode', 'pickupAlternative', 'pickupOverweight',
                                  'paymentMethodCode', 'pickupCardHolder', 'pickupCardType',
                                  'pickupCardNumber', 'pickupCardExpiry', 'pickupCardSecurity',
                                  'pickupCardAddress', 'pickupCardCountry', 'pickupPRN', 'trackingNumber',
                                  'pickupRoom', 'pickupFloor');
                break;
            case "USPS":
                $specific = array('userid', 'imageType', 'weightPounds',
                                  'weightOunces', 'firstClassMailType',
                                  'packagingType', 'pickupDate');
                break;
            case "FEDEX":
                $specific = array('key', 'packagingType', 'weightUnit', 'lengthUnit',
                                  'dropoffType', 'residential', 'paymentType',
                                  'labelFormatType', 'imageType', 'labelStockType',
                                  'packageCount', 'sequenceNumber', 'trackingIdType',
                                  'trackingNumber', 'shipmentIdentification',
                                  'pickupDate', 'signatureType', 'referenceCode',
                                  'referenceValue', 'smartPostIndicia', 'smartPostHubId',
                                  'smartPostEndorsement', 'smartPostSpecialServices',
                                  'insuredCurrency', 'insuredValue', 'saturdayDelivery',
                                  'residentialAddressIndicator', 'customsDocumentContent',
                                  'customsValue', 'customsNumberOfPieces',
                                  'countryOfManufacture', 'customsWeight',
                                  'customsCurrency', 'collectOnDelivery', 'codCollectionType',
                                  'codCollectionAmount', 'holdAtLocation', 'holdPhone', 'holdStreet',
                                  'holdCity', 'holdState', 'holdCode', 'holdCountry', 'holdResidential',
                                  'saturdayDelivery');
                break;
            case "STAMPS":
                $specific = array('weightPounds', 'imageType', 'packagingType',
                                  'declaredValue', 'customsContentType', 'customsComments',
                                  'customsLicenseNumber', 'customsCertificateNumber',
                                  'customsInvoiceNumber', 'customsOtherDescribe',
                                  'customsDescription', 'customsQuantity', 'customsValue',
                                  'customsWeight', 'customsHsTariff', 'customsOriginCountry',
                                  'insuredValue');
                break;
            default:
//                throw new RuntimeException("Invalid carrier '$carrier' in getOKparams");
        }
        return array_merge($generic, $specific);
    }

    /**
     * Gets defaults
     *
     * This function will grab defaults from config.php
     *
     * @throws RuntimeException
     * @param $param
     * @param $value
     * @param $carrier
     * @return value
     */
    public static function rocketshipit_getParameter($param, $value, $carrier)
    {
        // Force fedex, FedEx, FEDEX to all read the same
        $carrier = strtoupper($carrier);

        // If the default is not in the getOKparams function an exception is thrown
        if (!in_array($param, Shipping::rocketshipit_getOKparams($carrier)) && $param != '') {
            throw new Exception("Invalid parameter '$param' in setParameter");
        }

        if ($value == "") { // get the default, if set
            $value = \Config::get('generic.' . $param);
            if ($value == "") { // not in the generics? look in the specific carrier params
                switch ($carrier) {
                    case "UPS":
                        $value = \Config::get('ups.' . $param, false);
                        break;
                    case "USPS":
                        $value = getUSPSDefault($param);
                        break;
                    case "FEDEX":
                        $value = \Config::get('fedex.' . $param, false);
                        break;
                    case "STAMPS":
                        $value = getSTAMPSDefault($param);
                        break;
                    default:
//                        throw new RuntimeException("Unknown carrier in setParameter: '$carrier'");
                }
            }
        }
        return $value;
    }

    /**
     * Validates carrier name
     *
     * This function will return true when given a proper
     * carier name.
     * @throws RuntimeException
     * @param $carrier
     * @return bool
     */
    public static function rocketshipit_validateCarrier($carrier)
    {
        switch (strtoupper($carrier)) {
            case "UPS":
                return true;
            case "FEDEX":
                return true;
            case "USPS":
                return true;
            case "STAMPS":
                return true;
            default:
//                throw new RuntimeException("Unknown carrier in Shipping_Shipment: '$carrier'");
        }
    }

    // Takes a multi-dimensional array and makes a nested bulleted list
    function printArrayTree($array)
    {
        print "\n<ul>\n";
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                print "<li>$key</li>\n";
                Shipping::printArrayTree($value);
            } else {
                //print "<li>$value</li>\n";
            }
        }
        print "</ul>\n";
    }


}



/* end of file auth.php */
